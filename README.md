# QSpriteViewer

Half-Life /Quake/ Quake 2 sprite viewer

## Building from sources

QSpriteViewer can be built using Qt4 or Qt5. It's up to you to decide. 
In order to build QSpriteViewer you should have properly installed Qt4 (was tested on 4.8.6, but should work on all versions above 4.8.0) or Qt5 and conformant C++ compiler.

### Building on Linux

On Debian or deb-based distro run these commands at console:

    sudo apt-get install g++ libqt4-dev

or
    
    sudo apt-get install g++ qtbase5-dev

On Fedora or other rpm-based distro:

    sudo yum install gcc-c++ qt-devel

or

    sudo yum install gcc-c++ qt5-qtbase-devel

If you're not in *sudoers* run these commands as root without *sudo*. 
Other Linux distros may use different package manager and packages' names. 
If your distro doesn't use repositories or doesn't have Qt in it, you can compile Qt from sources or download prebuilt binaries compatible with your system. Please, see [qt-project](http://qt-project.org/downloads).

After Qt was installed, run these commands being in the **sources** directory:

    cd /path/to/sources
    qmake -r "CONFIG+=release" # or qmake-qt4
    make

Notice that *qmake* may refer to Qt4 or Qt5 tools depending on your system. Check qmake --version.

In case of success the file named *qspriteviewer* will be placed in *build/qt(4|5)/QSpriteViewer* directory.

### Building on Windows using MinGW

Obtain the copy of needed Qt version from [qt-project website](http://qt-project.org/downloads) or from [mingw-builds](http://sourceforge.net/projects/mingwbuilds/files/external-binary-packages/Qt-Builds/). For Qt4 the second option may be preferable since mingw-builds are shipped with all what you need, while official build comes separately from compilers and IDE.
After installation you can add ming32\bin and Qt\bin directories to your PATH environment variable for convenience. Next steps are the same as on Linux except you will need to use mingw32-make instead of make:

    cd path\to\sources
    qmake -r "CONFIG+=release"
	mingw32-make
	
In case of success the file named *qspriteviewer.exe* will be placed in *build/qt(4|5)/QSpriteViewer* directory.

### Building using QtCreator

You can build QSpriteViewer using official Qt IDE - QtCreator. Obtain QtCreator from your system repositories or from [qt-project website](http://qt-project.org/downloads).

Open *sources/project.pro* in QtCreator, go to **Projects** tab (on the left), choose preferred kit (if you have many), change build configuration to **Release** and turn off Shadow build (recommended). Then Build -> Build All.

### Building on other platforms

QSpriteViewer was not tested on other platforms, but since it uses Qt and no platform-specific functions it should be able to build QSpriteViewer on any platform where Qt is available. Source code is byteorder-aware, but it was not tested on big-endian systems.

## Distribution notes

### Linux notes

There is *generatedesktop.sh* file in the same directory with executable. This script generates **.desktop** file and install it to your home directory, so QSpriteViewer should appear in list of applications. To launch script type:

    sh generatedesktop.sh

### Windows notes

Windows applications are usually distributed with all non-system libraries they depend on. If you've built QSpriteViewer from sources and want to make portable archive, don't forget to put C++ runtime and Qt libraries in the same folder. Search your Qt installation *bin* directories to find libraries.
For Qt4/MinGW build required libraries are libstdc++-6.dll, libgcc_s_dw2-1.dll, libwinpthread-1.dll, libpng16-16.dll, zlib1.dll, QtCore4.dll, QtGui4.dll. 
For Qt5/MinGW build required libraries are libstdc++-6.dll, libgcc_s_dw2-1.dll, libwinpthread-1.dll, icuin52.dll, icuuc52.dll, icudt52.dll, Qt5Core.dll, Qt5Gui.dll, Qt5Widgets.dll.


