
CURRENT=$(dirname -- $(readlink -f -- $0))
QSPRITEVIEWER=$CURRENT/../build/qt4/QSpriteViewer/qspriteviewer

IMAGES="flame00.tga flame01.tga flame02.tga flame03.tga flame04.tga flame05.tga"

$QSPRITEVIEWER --console create-sprite --game q2 --output q2sprite.sp2 $IMAGES
$QSPRITEVIEWER --console create-sprite --game q --output qsprite.spr $IMAGES
$QSPRITEVIEWER --console create-sprite --game hl --palette flame04.tga --output sprite.spr $IMAGES


$QSPRITEVIEWER --console info q2sprite.sp2
$QSPRITEVIEWER --console info qsprite.spr
$QSPRITEVIEWER --console info sprite.spr


$QSPRITEVIEWER --console export-pal sprite.spr --output sprite.pal
$QSPRITEVIEWER --console export-pal qsprite.spr --output qsprite.pal

$QSPRITEVIEWER --console export-sequence sprite.spr --format jpg --basename hlsprite --frames 2-4
