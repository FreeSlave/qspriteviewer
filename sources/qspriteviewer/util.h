/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef SPRITEUTIL_H
#define SPRITEUTIL_H

#include <QIODevice>
#include <QString>

template<typename T>
bool readStruct(QIODevice* device, T& t)
{
    char* bytes = reinterpret_cast<char*>(&t);
    return device->read(bytes, sizeof(T)) == sizeof(T);
}

template<typename T>
bool readStructs(QIODevice* device, T* t, int number)
{
    char* bytes = reinterpret_cast<char*>(t);
    return device->read(bytes, sizeof(T)*number) == sizeof(T)*number;
}

template<typename T>
bool writeStruct(QIODevice* device, const T& t)
{
    const char* bytes = reinterpret_cast<const char*>(&t);
    return device->write(bytes, sizeof(T)) == sizeof(T);
}

template<typename T>
bool writeBytes(QIODevice* device, const T* arr, int size)
{
    const char* bytes = reinterpret_cast<const char*>(arr);
    return device->write(bytes, sizeof(T)*size) == sizeof(T)*size;
}

QString addExtension(QString fileName, QString extension);

int numberOfDigits(int n);

#endif // SPRITEUTIL_H
