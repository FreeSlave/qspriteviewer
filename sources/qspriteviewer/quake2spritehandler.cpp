/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include <QBuffer>
#include <QByteArray>
#include <QCoreApplication>
#include <QDir>
#include <QFileInfo>
#include <QImageReader>
#include <QImageWriter>
#include <QMap>
#include <QPainter>
#include <QtEndian>

#include "pakfile.h"
#include "quakespritehandler.h"
#include "quake2spritehandler.h"
#include "spritegn.h"
#include "util.h"

Quake2SpriteHandler::Quake2SpriteHandler()
{
}

QString Quake2SpriteHandler::formatName() const
{
    return "Quake 2";
}

QString Quake2SpriteHandler::extension() const
{
    return "sp2";
}

QString Quake2SpriteHandler::shortIdentifier() const
{
    return "q2";
}

bool Quake2SpriteHandler::isPaletteRequired() const
{
    return false;
}

QVector<QRgb> Quake2SpriteHandler::defaultPalette() const
{
    QVector<QRgb> palette(256);
    const quint8* pal = quakePalette();
    for (int i=0; i<palette.size(); ++i)
        palette[i] = qRgba(pal[i*3+0], pal[i*3+1], pal[i*3+2], 0xFF);
    return palette;
}

bool Quake2SpriteHandler::canLoad(QIODevice *device) const
{
    int ident;
    if (readStruct(device, ident))
    {
        ident = qFromLittleEndian(ident);
        if (ident == IDSPRITEHEADER2)
            return true;
    }
    return false;
}

Sprite Quake2SpriteHandler::load(QIODevice *device, const QString &fileName) const
{
    Sprite sprite;

    dsprite2_t q2header;
    if (!readStruct(device, q2header))
    {
        sprite.setErrorString(QCoreApplication::translate("Sprite", "Could not read sprite header"));
        return sprite;
    }
    makeHostEndians(q2header);

    QVector<SpriteFrame> frames(q2header.numframes);
    QVector<Quake2SpriteFrameInfo> frameInfos(q2header.numframes);

    for (int i=0; i<q2header.numframes; ++i)
    {
        dsprite2frame_t frameStruct;
        if (!readStruct(device, frameStruct))
        {
            sprite.setErrorString(QCoreApplication::translate("Sprite", "Could not read frame info"));
            return sprite;
        }
        makeHostEndians(frameStruct);

        Quake2SpriteFrameInfo info;
        info.origin = QPoint(frameStruct.origin_x, frameStruct.origin_y);
        info.name = frameStruct.name;
        frameInfos[i] = info;
    }

    if (fileName.isEmpty()) //probably loaded from pak file
    {
        if (device->seek(0))
        {
            int ident;
            if (!readStruct(device, ident))
            {
                return sprite;
            }

            if (ident != IDPAKHEADER)
            {
                return sprite;
            }

            dlump_t dlump;
            if (!readStruct(device, dlump))
            {
                return sprite;
            }
            if (!device->seek(dlump.dirofs))
            {
                return sprite;
            }

            int numLumps = dlump.dirlen / sizeof(lump_t);
            QMap<QString, QSize> lumpInfos;

            for (int i=0; i<numLumps; ++i)
            {
                lump_t lump;
                if (!readStruct(device, lump))
                {
                    return sprite;
                }
                else
                {
                    QString name = lump.name;
                    foreach (Quake2SpriteFrameInfo info, frameInfos)
                    {
                        if (info.name == name)
                        {
                            lumpInfos[name] = QSize(lump.filelen, lump.filepos);
                            break;
                        }
                    }
                }
            }
            if (lumpInfos.size() != q2header.numframes)
            {
                sprite.setErrorString(QCoreApplication::translate("Sprite", "Could not find all frames"));
                return sprite;
            }
            for (int i=0; i<frameInfos.size(); ++i)
            {
                Quake2SpriteFrameInfo info = frameInfos[i];
                QSize pos = lumpInfos[info.name];
                int len = pos.width();
                int offset = pos.height();

                device->seek(offset);
                QByteArray arr = device->read(len);
                QBuffer buffer(&arr);
                buffer.open(QIODevice::ReadOnly);

                QImageReader reader(&buffer);
                QImage image = reader.read();
                if (image.isNull())
                {
                    sprite.setErrorString(QCoreApplication::translate("Sprite", "Could not load frame file %1: %2").arg(info.name, reader.errorString()));
                    return sprite;
                }
                SpriteFrame frame;
                frame.image = image;
                frame.info = QVariant::fromValue(info);
                frames[i] = frame;
            }
        }
        else
        {
            return sprite;
        }
    }
    else
    {
        for (int i=0; i<frameInfos.size(); ++i)
        {
            Quake2SpriteFrameInfo info = frameInfos[i];
            QString frameFileName = QFileInfo(fileName).dir().filePath(QFileInfo(info.name).fileName());
            QImageReader reader(frameFileName);
            QImage image = reader.read();
            if (image.isNull())
            {
                sprite.setErrorString(QCoreApplication::translate("Sprite", "Could not load frame file %1: %2").arg(frameFileName, reader.errorString()));
                return sprite;
            }
            SpriteFrame frame;
            frame.image = image;
            frame.info = QVariant::fromValue(info);
            frames[i] = frame;
        }
    }

    sprite.set(this, QVariant(), frames, fileName);
    return sprite;
}

QString Quake2SpriteHandler::write(const Sprite &sprite, QIODevice *device, const QString &fileName) const
{
    dsprite2_t header;
    header.ident = IDSPRITEHEADER2;
    header.version = SPRITE_QUAKE2_VERSION;
    header.numframes = sprite.frameCount();

    makeLittleEndians(header);
    if (!writeStruct(device, header))
        return QCoreApplication::translate("Sprite", "Could not write sprite header");

    QByteArray pcx("pcx");
    if (!QImageWriter::supportedImageFormats().contains(pcx))
        return QCoreApplication::translate("Sprite", "pcx writing required by Quake 2 sprite format is not supported. Probably missing plugin?");

    QFileInfo fileInfo(fileName);
    QString baseName = fileInfo.baseName();
    QDir dir = fileInfo.dir();

    for (int i=0; i<sprite.frameCount(); ++i)
    {
        SpriteFrame spriteFrame = sprite.frame(i);
        Quake2SpriteFrameInfo frameInfo = spriteFrame.info.value<Quake2SpriteFrameInfo>();

        dsprite2frame_t frameStruct;
        frameStruct.width = spriteFrame.image.width();
        frameStruct.height = spriteFrame.image.height();
        frameStruct.origin_x = frameInfo.origin.x();
        frameStruct.origin_y = frameInfo.origin.y();

        QString baseFrameName = QString("%1%2.pcx").arg(baseName).arg(i);
        QByteArray frameName = QString("sprites/%1").arg(baseFrameName).toLocal8Bit();

        if (frameName.size() >= static_cast<int>(sizeof(frameStruct.name)))
            return QCoreApplication::translate("Sprite", "Too long Quake 2 sprite name");

        strncpy(frameStruct.name, frameName.constData(), frameName.size());
        frameStruct.name[frameName.size()] = '\0';

        makeLittleEndians(frameStruct);
        if (!writeStruct(device, frameStruct))
            return QCoreApplication::translate("Sprite", "Could not write frame info");

        QString imageFileName = dir.filePath(baseFrameName);

        QImageWriter writer(imageFileName, pcx);
        if (!writer.write(spriteFrame.image))
            return QCoreApplication::translate("Sprite", "%1: failed to write image").arg(imageFileName);
    }

    return QString();
}

QStringList Quake2SpriteHandler::stringSpriteInfo(const Sprite &sprite) const
{
    QStringList infoList;
    infoList << QCoreApplication::translate("spriteInfo", "Version: %1").arg(formatName());
    infoList << QCoreApplication::translate("spriteInfo", "Total frames: %1").arg(sprite.frameCount());
    return infoList;
}

QStringList Quake2SpriteHandler::stringFrameInfo(const SpriteFrame &frame) const
{
    Quake2SpriteFrameInfo frameInfo = frame.info.value<Quake2SpriteFrameInfo>();

    QStringList infoList;
    infoList << QCoreApplication::translate("spriteInfo", "Origin: (%1, %2)").arg(frameInfo.origin.x()).arg(frameInfo.origin.y());
    infoList << QCoreApplication::translate("spriteInfo", "Size: %1 x %2").arg(frame.image.width()).arg(frame.image.height());
    infoList << QCoreApplication::translate("spriteInfo", "Name: %1").arg(frameInfo.name);
    return infoList;
}

QVariant Quake2SpriteHandler::generateFrameInfo(const QImage &image) const
{
    Quake2SpriteFrameInfo frameInfo;
    frameInfo.origin = QPoint(image.width()/2, image.height()/2);
    return QVariant::fromValue(frameInfo);
}

QVariant Quake2SpriteHandler::generateSpriteInfo(const QVector<SpriteFrame> &frames) const
{
    Q_UNUSED(frames);
    return QVariant();
}

QIcon Quake2SpriteHandler::icon() const
{
    return QIcon(":/images/quake2.png");
}

int Quake2SpriteHandler::requiredPaletteSize() const
{
    return 256;
}

QImage Quake2SpriteHandler::convertImage(const QImage &image, const QVector<QRgb> &palette) const
{
    return image.convertToFormat(QImage::Format_Indexed8, palette);
}

QPixmap Quake2SpriteHandler::makeTransparent(const QImage &image, const QColor &background, const QVariant &spriteInfo) const
{
    Q_UNUSED(background)
    Q_UNUSED(spriteInfo)
    QImage myImage = image;
    if (myImage.colorCount() == 256)
        myImage.setColor(255, qRgba(0,0,0,0));

    return QPixmap::fromImage(myImage);
}

QDataStream &operator<<(QDataStream &out, const Quake2SpriteFrameInfo &myObj)
{
    out << myObj.origin << myObj.name;
    return out;
}

QDataStream &operator>>(QDataStream &in, Quake2SpriteFrameInfo &myObj)
{
    in >> myObj.origin >> myObj.name;
    return in;
}
