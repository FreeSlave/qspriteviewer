/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include "spritelabel.h"
#include "spritehandler.h"

const int SpriteLabel::defaultFps = 10;
const int SpriteLabel::defaultScale = 100;

SpriteLabel::SpriteLabel(QWidget *parent) :
    QLabel(parent)
{
    _fps = defaultFps;
    _currentFrame = 0;
    _running = false;
    _transparent = false;
    _scale = defaultScale;

    _timer = startTimer(1000/_fps);
}

void SpriteLabel::setSprite(const Sprite &sprite)
{
    _sprite = sprite;
    resetState();
}

QPixmap SpriteLabel::currentPixmap() const
{
    return _pixmaps.at(_currentFrame);
}

void SpriteLabel::recachePixmaps()
{
    if (_sprite.isValid())
    {
        _pixmaps.resize(_sprite.frameCount());
        for (int i=0; i<_pixmaps.size(); ++i)
        {
            QImage image = _sprite.frameImage(i);
            QPixmap pixmap;

            if (_transparent)
                pixmap = _sprite.handler()->makeTransparent(image, palette().background().color(), _sprite.info());
            else
                pixmap = QPixmap::fromImage(image);

            if (_scale != defaultScale)
            {
                float scale = static_cast<float>(_scale)/100;
                QSize size = QSizeF(image.width()*scale, image.height()*scale).toSize();
                pixmap = pixmap.scaled(size, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
            }
            _pixmaps[i] = pixmap;
        }
    }
}

void SpriteLabel::setFps(int fps)
{
    if (_fps != fps && fps > 0)
    {
        _fps = fps;
        killTimer(_timer);
        _timer = startTimer(1000/_fps);
        emit fpsChanged(_fps);
    }
}

void SpriteLabel::setScale(int percent)
{
    if (_scale != percent && percent > 0)
    {
        _scale = percent;
        recachePixmaps();
        updateCurrentFrame();
        emit scaleChanged(_scale);
    }
}

void SpriteLabel::setRunning(bool running)
{
    _running = running;
}

void SpriteLabel::setCurrentFrame(int i)
{
    if (_sprite.isValid() && _currentFrame != i && i >= 0 && i < _pixmaps.size())
    {
        _currentFrame = i;
        updateCurrentFrame();
        if (i == _pixmaps.size() - 1 && isRunning())
            emit finished();
    }
}

void SpriteLabel::updateCurrentFrame()
{
    if (_sprite.isValid())
    {
        setPixmap(_pixmaps.at(_currentFrame));
        emit frameUpdated(_currentFrame);
    }
}

void SpriteLabel::start()
{
    setRunning(true);
}

void SpriteLabel::stop()
{
    setRunning(false);
}

void SpriteLabel::nextFrame()
{
    int i =_currentFrame+1;
    if (_sprite.frameCount() == i)
        i = 0;
    setCurrentFrame(i);
}

void SpriteLabel::prevFrame()
{
    int i =_currentFrame-1;
    if (i < 0)
        i = _sprite.frameCount()-1;
    setCurrentFrame(i);
}

void SpriteLabel::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);
    if (_sprite.isValid() && _sprite.isAnimated() && _running)
    {
        nextFrame();
    }
}

void SpriteLabel::resetState()
{
    _currentFrame = 0;
    recachePixmaps();
    updateCurrentFrame();
}

void SpriteLabel::removeSprite()
{
    _sprite = Sprite();
    _pixmaps.clear();
}

void SpriteLabel::setTransparent(bool transparent)
{
    if (transparent != _transparent)
    {
        _transparent = transparent;
        recachePixmaps();
        updateCurrentFrame();
    }
}
