/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include "pakdialog.h"
#include <QDialogButtonBox>
#include <QVBoxLayout>

PakDialog::PakDialog(const QVector<QPair<QString, QSize> >& entries, QWidget *parent) :
    QDialog(parent)
{
    _listWidget = new QListWidget;

    for (int i=0; i<entries.size(); ++i)
    {
        QPair<QString, QSize> entry = entries.at(i);
        QListWidgetItem* item = new QListWidgetItem(entry.first);
        item->setData(Qt::UserRole, entry.second);
        _listWidget->addItem(item);
    }

    QDialogButtonBox* buttonBox = new QDialogButtonBox;
    buttonBox->addButton(QDialogButtonBox::Ok);
    buttonBox->addButton(QDialogButtonBox::Cancel);

    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addWidget(_listWidget);
    vbox->addWidget(buttonBox);

    setLayout(vbox);

    connect(_listWidget, SIGNAL(itemClicked(QListWidgetItem*)), SLOT(slotItemClicked(QListWidgetItem*)));
    connect(_listWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), SLOT(slotItemDoubleClicked(QListWidgetItem*)));
    connect(buttonBox, SIGNAL(accepted()), SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), SLOT(reject()));
}

QPair<QString, QSize> PakDialog::chosen() const
{
    return _chosen;
}

void PakDialog::slotItemClicked(QListWidgetItem *item)
{
    _chosen = qMakePair(item->text(), item->data(Qt::UserRole).toSize());
}

void PakDialog::slotItemDoubleClicked(QListWidgetItem *item)
{
    _chosen = qMakePair(item->text(), item->data(Qt::UserRole).toSize());
    accept();
}
