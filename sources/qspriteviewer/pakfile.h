/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef PAKFILE_H
#define PAKFILE_H

#include <QIODevice>
#include <QPair>
#include <QSize>
#include <QString>
#include <QVector>

typedef struct
{
    char name[56];
    int filepos;
    int filelen;
} lump_t;

typedef struct
{
    int dirofs;
    int dirlen;
} dlump_t;

enum {
    IDPAKHEADER = (('K'<<24)+('C'<<16)+('A'<<8)+'P')
};

QVector<QPair<QString, QSize> > pakSpriteEntries(QIODevice* device, QString& error);

#endif // PAKFILE_H
