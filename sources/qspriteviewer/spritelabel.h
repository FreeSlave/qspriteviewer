/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef SPRITELABEL_H
#define SPRITELABEL_H

#include <QBitmap>
#include <QColor>
#include <QLabel>
#include <QPixmap>
#include <QVector>

#include "sprite.h"

class SpriteLabel : public QLabel
{
    Q_OBJECT
public:
    explicit SpriteLabel(QWidget *parent = 0);
    explicit SpriteLabel(const Sprite& sprite, QWidget *parent = 0);

    inline const Sprite& sprite() const {
        return _sprite;
    }
    void setSprite(const Sprite& sprite);

    inline int fps() const {
        return _fps;
    }
    inline int scale() const {
        return _scale;
    }

    int currentFrameNumber() const {
        return _currentFrame;
    }
    QPixmap currentPixmap() const;

    bool isRunning() const {
        return _running;
    }

    bool isTransparent() const {
        return _transparent;
    }

    void removeSprite();

    static const int defaultScale;
    static const int defaultFps;

signals:
    void fpsChanged(int);
    void scaleChanged(int);
    void frameUpdated(int);
    void finished();

public slots:
    void setFps(int fps);
    void setScale(int percent);
    void setRunning(bool running);
    void setTransparent(bool transparent);
    void setCurrentFrame(int i);

    void start();
    void stop();

    void nextFrame();
    void prevFrame();

protected:
    void timerEvent(QTimerEvent *event);

private:
    void recachePixmaps();
    void resetState();
    void updateCurrentFrame();

    int _fps;
    int _scale;
    int _currentFrame;
    int _timer;
    bool _running;
    bool _transparent;

    Sprite _sprite;
    QColor _backGroundColor;
    QVector<QPixmap> _pixmaps;
};

#endif // SPRITELABEL_H
