#include "jascpalettehandler.h"

#include <cstring>
#include <cstdio>
#include <cstdlib>

namespace
{
const char* const JASC_PAL = "JASC-PAL";
}

JASCPaletteHandler::JASCPaletteHandler()
{
}

QString JASCPaletteHandler::name() const
{
    return "JASC palette";
}

QString JASCPaletteHandler::extension() const
{
    return "pal";
}

bool JASCPaletteHandler::canLoad(QIODevice *device) const
{
    if (device && device->isOpen() && device->isReadable())
    {
        char buf[64] = {'\0'};
        device->readLine(buf, sizeof(buf));
        if (strncmp(buf, "JASC-PAL", 8) == 0)
            return true;
    }
    return false;
}

QVector<QRgb> JASCPaletteHandler::readFromDevice(QIODevice *device) const
{
    char buf[64] = {'\0'};
    device->readLine(buf, sizeof(buf));
    device->readLine(buf, sizeof(buf)); //just skip 0100

    device->readLine(buf, sizeof(buf));
    long size = strtol(buf, NULL, 10);

    if (size <= 0)
        return QVector<QRgb>();

    QVector<QRgb> pal(size);
    for (int i=0; i<size; ++i)
    {
        qint64 n = device->readLine(buf, sizeof(buf));
        if (n <= 0)
            return QVector<QRgb>();

        char* endptr;
        long red = strtol(buf, &endptr, 10);
        long green = strtol(endptr, &endptr, 10);
        long blue = strtol(endptr, NULL, 10);

        if (red < 0 || red >= 256 || green < 0 || green >= 256 || blue < 0 || blue >= 256)
            return QVector<QRgb>();

        pal[i] = qRgb(red, green, blue);
    }
    return pal;
}

bool JASCPaletteHandler::writeToDevice(QIODevice *device, const QVector<QRgb> &pal) const
{
    if (device && device->isOpen() && device->isWritable())
    {
        char buf[64];
        if (device->write(JASC_PAL) <= 0)
            return false;
        if (device->write("\r\n0100\r\n") <= 0)
            return false;
        sprintf(buf, "%d\r\n", pal.size());
        if (device->write(buf) <=0 )
            return false;
        for (int i=0; i<pal.size(); ++i)
        {
            QRgb rgb = pal[i];
            sprintf(buf, "%d %d %d\r\n", qRed(rgb), qGreen(rgb), qBlue(rgb));
            if (device->write(buf) <= 0)
                return false;
        }
        return true;
    }
    return false;
}
