/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef EXPORTSPRITE_H
#define EXPORTSPRITE_H

#include <QByteArray>
#include <QChar>
#include <QImage>
#include <QString>
#include <QVector>

#include "sprite.h"

QString defaultPattern();
QChar defaultFillChar();
QVector<int> parseFrameRange(const QString& frameRange, int frameCount, int first = 0, QString* error = 0);
QString fromPattern(const QString& pattern, const QString& baseName, int i, int fillWidth, QChar fillChar, const QString& extension);
bool exportFrame(const QImage& image, const QString& filePath, const QByteArray& format, QString* error = 0);
bool exportSprite(const Sprite& sprite, QVector<int> indices, const QString& path, const QString& baseName,
                    const QString& pattern, int fillWidth, QChar fillChar, const QByteArray& format, QString* error = 0);

#endif // EXPORTSPRITE_H
