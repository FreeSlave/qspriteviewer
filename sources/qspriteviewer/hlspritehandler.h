/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef HLSPRITEHANDLER_H
#define HLSPRITEHANDLER_H

#include <QMetaType>
#include <QDataStream>
#include "quakespritehandler.h"

class HLSpriteHandler : public QuakeSpriteHandler
{
public:
    HLSpriteHandler();

    QString formatName() const;
    QString extension() const;
    QString shortIdentifier() const;
    bool isPaletteRequired() const;
    QVector<QRgb> defaultPalette() const;
    bool canLoad(QIODevice* device) const;
    Sprite load(QIODevice* device, const QString& fileName = QString()) const;
    QString write(const Sprite& sprite, QIODevice* device, const QString& = QString()) const;
    QStringList stringSpriteInfo(const Sprite& sprite) const;
    QVariant generateSpriteInfo(const QVector<SpriteFrame>& frames) const;
    QIcon icon() const;
    QPixmap makeTransparent(const QImage& image, const QColor& background, const QVariant& spriteInfo) const;

    QVector<QRgb> readPalette(QIODevice* device, int texFormat, QString& error) const;

    static QString texFormatName(int texFormat);
};

struct HLSpriteInfo
{
    HLSpriteInfo();
    QSize size;
    int texFormat;
    int faceType;
    float boundingRadius;
};

QDataStream &operator<<(QDataStream &out, const HLSpriteInfo &myObj);
QDataStream &operator>>(QDataStream &in, HLSpriteInfo &myObj);

Q_DECLARE_METATYPE(HLSpriteInfo)

#endif // HLSPRITEHANDLER_H
