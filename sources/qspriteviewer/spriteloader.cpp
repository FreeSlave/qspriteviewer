/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include "spriteloader.h"

#include <QCoreApplication>
#include <QFile>

QList<const SpriteHandler*> SpriteLoader::_handlers;

void SpriteLoader::addHandler(const SpriteHandler *handler)
{
    _handlers.append(handler);
}

Sprite SpriteLoader::load(QIODevice *device, const QString &fileName)
{
    for (int i=0; i<_handlers.size(); ++i)
    {
        const SpriteHandler* handler = _handlers.at(i);
        qint64 pos = device->pos();
        bool b = handler->canLoad(device);
        device->seek(pos);
        if (b)
            return handler->load(device, fileName);
    }
    Sprite sprite;
    sprite.setErrorString(QCoreApplication::translate("Sprite", "Unknown sprite format"));
    return sprite;
}

Sprite SpriteLoader::load(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
    {
        Sprite sprite;
        sprite.setErrorString(file.errorString());
        return sprite;
    }
    else
        return SpriteLoader::load(&file, fileName);
}

QList<const SpriteHandler*> SpriteLoader::handlers()
{
    return _handlers;
}

const SpriteHandler* SpriteLoader::handlerByName(const QString& name)
{
    for (int i=0; i<_handlers.size(); ++i)
    {
        if (_handlers.at(i)->formatName() == name)
            return _handlers.at(i);
    }
    return 0;
}

const SpriteHandler* SpriteLoader::handlerByShortIdentifier(const QString &id)
{
    for (int i=0; i<_handlers.size(); ++i)
    {
        if (_handlers.at(i)->shortIdentifier() == id)
            return _handlers.at(i);
    }
    return 0;
}
