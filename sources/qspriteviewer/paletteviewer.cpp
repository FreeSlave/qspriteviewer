/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include "paletteviewer.h"
#include "exportpalette.h"
#include "paletteloader.h"

#include <QApplication>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QInputDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>

PaletteViewer::PaletteViewer(const QVector<QRgb> &colormap, QWidget *parent) :
    QWidget(parent)
{
    _currentIndexLabel = new QLabel;
    _colorEdit = new QLineEdit;
    _colorEdit->setReadOnly(true);

    QFormLayout* formLayout = new QFormLayout;
    formLayout->addRow(tr("Index: "), _currentIndexLabel);
    formLayout->addRow(tr("Color: "), _colorEdit);

    _palWidget = new PaletteWidget(colormap);
    _palWidget->setFocusPolicy(Qt::StrongFocus);
    _palWidget->setMinimumSize(_palWidget->sizeHint());

    QHBoxLayout* hbox = new QHBoxLayout;
    hbox->addWidget(_palWidget);
    hbox->addLayout(formLayout);
    hbox->addStretch();
    setLayout(hbox);

    connect(_palWidget, SIGNAL(colorChosen(int,QRgb)), SLOT(colorSlot(int,QRgb)));
    connect(_colorEdit, SIGNAL(editingFinished()), SLOT(changeCurrentColor()));
}

void PaletteViewer::setEditable(bool editable)
{
    _colorEdit->setReadOnly(!editable);
}

bool PaletteViewer::isEditable() const
{
    return !_colorEdit->isReadOnly();
}

void PaletteViewer::colorSlot(int index, QRgb rgb)
{
    if (index >= 0)
    {
        _currentIndexLabel->setText(QString::number(index));
        _colorEdit->setText(colorToText(rgb));
    }
    else
    {
        _currentIndexLabel->clear();
        _colorEdit->clear();
    }
}

void PaletteViewer::exportPalette()
{
    QStringList formats;
    foreach (const PaletteHandler* handler, PaletteLoader::handlers()) {
        formats << handler->name();
    }

    bool ok;
    QString format = QInputDialog::getItem(this, tr("Choose palette format"), tr("Palette format"), formats, 0, false, &ok);

    if (!ok)
        return;

    const PaletteHandler* handler = PaletteLoader::handlerByName(format);
    if (!handler)
        return;

    QFileInfo fileInfo(_filePath);
    QString basename = fileInfo.baseName().isEmpty() ? "palette" : fileInfo.baseName();
    QString filePath = QString("%1.%2").arg(fileInfo.dir().filePath(basename), handler->extension());
    filePath = QFileDialog::getSaveFileName(this, tr("Export palette"), filePath, QString("%1 (*.%2);;Any (*)").arg(handler->name(), handler->extension()));

    if (!filePath.isEmpty())
    {
        QString error;
        if (!::exportPalette(_palWidget->colorTable(), filePath, handler, &error))
        {
            QMessageBox::critical(this, qApp->applicationName(), tr("Failed to save palette: %1").arg(error));
        }
    }
}

void PaletteViewer::changeCurrentColor()
{
    QColor color = colorFromText(_colorEdit->text());
    if (color.isValid())
        _palWidget->setColor(_palWidget->currentIndex(), color.rgba());
}

QString PaletteViewer::colorToText(const QRgb &rgb)
{
    return QString("%1 %2 %3").arg(qRed(rgb)).arg(qGreen(rgb)).arg(qBlue(rgb));
}

QColor PaletteViewer::colorFromText(const QString &text)
{
    QStringList list = text.split(' ', QString::SkipEmptyParts);
    QColor toReturn;
    if (list.size() == 3)
    {
        bool ok;
        int red = list.at(0).toInt(&ok);
        if (!ok || red < 0 || red > 255)
            return toReturn;
        int green = list.at(1).toInt(&ok);
        if (!ok || green < 0 || green > 255)
            return toReturn;
        int blue = list.at(2).toInt(&ok);
        if (!ok || blue < 0 || blue > 255)
            return toReturn;

        toReturn.setRed(red);
        toReturn.setGreen(green);
        toReturn.setBlue(blue);

        return toReturn;
    }
    return toReturn;
}
