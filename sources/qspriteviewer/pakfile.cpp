/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include <QFileInfo>
#include <QCoreApplication>

#include <cstring>

#include "pakfile.h"
#include "spriteloader.h"
#include "util.h"

QVector<QPair<QString, QSize> > pakSpriteEntries(QIODevice* device, QString &error)
{
    QVector<QPair<QString, QSize> > empty;
    QVector<QPair<QString, QSize> > toReturn;

    int ident;
    if (!readStruct(device, ident))
    {
        error = QCoreApplication::translate("pakFile", "Could not read id");
        return empty;
    }

    if (ident != IDPAKHEADER)
    {
        error = QCoreApplication::translate("pakFile", "Wrong pak id");
        return empty;
    }

    dlump_t dlump;
    if (!readStruct(device, dlump))
    {
        error = QCoreApplication::translate("pakFile", "Could not read directory information");
        return empty;
    }
    if (!device->seek(dlump.dirofs))
    {
        error = QCoreApplication::translate("pakFile", "Could not seek to directories offset");
        return empty;
    }

    QStringList extensions;
    QList<const SpriteHandler*> handlers = SpriteLoader::handlers();
    foreach (const SpriteHandler* handler, handlers) {
        QString extension = handler->extension();
        if (!extensions.contains(extension))
            extensions.append(extension);
    }

    int numLumps = dlump.dirlen / sizeof(lump_t);
    for (int i=0; i<numLumps; ++i)
    {
        lump_t lump;
        if (!readStruct(device, lump))
        {
            error = QCoreApplication::translate("pakFile", "Could not read file information");
            return empty;
        }
        else
        {
            QString name = lump.name;
            if (extensions.contains(QFileInfo(name).suffix()))
            {
                toReturn.append(qMakePair(name, QSize(lump.filelen, lump.filepos)));
            }
        }
    }
    return toReturn;
}
