/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include <QCoreApplication>
#include <QPainter>
#include <QtEndian>

#include <cmath>

#include "quakespritehandler.h"
#include "spritegn.h"
#include "util.h"


const quint8* quakePalette()
{
    static quint8 palette[] =
    {
    // marked: colormap colors: cb = (colormap & 0xF0);cb += (cb >= 128 && cb < 224) ? 4 : 12;
    // 0x0*
        0,0,0,        15,15,15,     31,31,31,     47,47,47,     63,63,63,     75,75,75,     91,91,91,     107,107,107,
        123,123,123,  139,139,139,  155,155,155,  171,171,171,  187,187,187,  203,203,203,  219,219,219,  235,235,235,
    // 0x1*                                                   0 ^
        15,11,7,      23,15,11,     31,23,11,     39,27,15,     47,35,19,     55,43,23,     63,47,23,     75,55,27,
        83,59,27,     91,67,31,     99,75,31,     107,83,31,    115,87,31,    123,95,35,    131,103,35,   143,111,35,
    // 0x2*                                                   1 ^
        11,11,15,     19,19,27,     27,27,39,     39,39,51,     47,47,63,     55,55,75,     63,63,87,     71,71,103,
        79,79,115,    91,91,127,    99,99,139,    107,107,151,  115,115,163,  123,123,175,  131,131,187,  139,139,203,
    // 0x3*                                                   2 ^
        0,0,0,        7,7,0,        11,11,0,      19,19,0,      27,27,0,      35,35,0,      43,43,7,      47,47,7,
        55,55,7,      63,63,7,      71,71,7,      75,75,11,     83,83,11,     91,91,11,     99,99,11,     107,107,15,
    // 0x4*                                                   3 ^
        7,0,0,        15,0,0,       23,0,0,       31,0,0,       39,0,0,       47,0,0,       55,0,0,       63,0,0,
        71,0,0,       79,0,0,       87,0,0,       95,0,0,       103,0,0,      111,0,0,      119,0,0,      127,0,0,
    // 0x5*                                                   4 ^
        19,19,0,      27,27,0,      35,35,0,      47,43,0,      55,47,0,      67,55,0,      75,59,7,      87,67,7,
        95,71,7,      107,75,11,    119,83,15,    131,87,19,    139,91,19,    151,95,27,    163,99,31,    175,103,35,
    // 0x6*                                                   5 ^
        35,19,7,      47,23,11,     59,31,15,     75,35,19,     87,43,23,     99,47,31,     115,55,35,    127,59,43,
        143,67,51,    159,79,51,    175,99,47,    191,119,47,   207,143,43,   223,171,39,   239,203,31,   255,243,27,
    // 0x7*                                                   6 ^
        11,7,0,       27,19,0,      43,35,15,     55,43,19,     71,51,27,     83,55,35,     99,63,43,     111,71,51,
        127,83,63,    139,95,71,    155,107,83,   167,123,95,   183,135,107,  195,147,123,  211,163,139,  227,179,151,
    // 0x8*                                                   7 ^        v 8
        171,139,163,  159,127,151,  147,115,135,  139,103,123,  127,91,111,   119,83,99,    107,75,87,    95,63,75,
        87,55,67,     75,47,55,     67,39,47,     55,31,35,     43,23,27,     35,19,19,     23,11,11,     15,7,7,
    // 0x9*                                                   9 v
        187,115,159,  175,107,143,  163,95,131,   151,87,119,   139,79,107,   127,75,95,    115,67,83,    107,59,75,
        95,51,63,     83,43,55,     71,35,43,     59,31,35,     47,23,27,     35,19,19,     23,11,11,     15,7,7,
    // 0xA*                                                  10 v
        219,195,187,  203,179,167,  191,163,155,  175,151,139,  163,135,123,  151,123,111,  135,111,95,   123,99,83,
        107,87,71,    95,75,59,     83,63,51,     67,51,39,     55,43,31,     39,31,23,     27,19,15,     15,11,7,
    // 0xB*                                                  11 v
        111,131,123,  103,123,111,  95,115,103,   87,107,95,    79,99,87,     71,91,79,     63,83,71,     55,75,63,
        47,67,55,     43,59,47,     35,51,39,     31,43,31,     23,35,23,     15,27,19,     11,19,11,     7,11,7,
    // 0xC*                                                  12 v
        255,243,27,   239,223,23,   219,203,19,   203,183,15,   187,167,15,   171,151,11,   155,131,7,    139,115,7,
        123,99,7,     107,83,0,     91,71,0,      75,55,0,      59,43,0,      43,31,0,      27,15,0,      11,7,0,
    // 0xD*                                                  13 v
        0,0,255,      11,11,239,    19,19,223,    27,27,207,    35,35,191,    43,43,175,    47,47,159,    47,47,143,
        47,47,127,    47,47,111,    47,47,95,     43,43,79,     35,35,63,     27,27,47,     19,19,31,     11,11,15,
    // 0xE*
        43,0,0,       59,0,0,       75,7,0,       95,7,0,       111,15,0,     127,23,7,     147,31,7,     163,39,11,
        183,51,15,    195,75,27,    207,99,43,    219,127,59,   227,151,79,   231,171,95,   239,191,119,  247,211,139,
    // 0xF*                                                  14 ^
        167,123,59,   183,155,55,   199,195,55,   231,227,87,   127,191,255,  171,231,255,  215,255,255,  103,0,0,
        139,0,0,      179,0,0,      215,0,0,      255,0,0,      255,243,147,  255,247,199,  255,255,255,  159,91,83
    }; //

    return palette;
}

QuakeSpriteInfo::QuakeSpriteInfo() : faceType(SPR_VP_PARALLEL), boundingRadius(0.0f)
{

}

QuakeSpriteHandler::QuakeSpriteHandler()
{
}

QString QuakeSpriteHandler::formatName() const
{
    return "Quake";
}

QString QuakeSpriteHandler::extension() const
{
    return "spr";
}

QString QuakeSpriteHandler::shortIdentifier() const
{
    return "q";
}

bool QuakeSpriteHandler::isPaletteRequired() const
{
    return false;
}

QVector<QRgb> QuakeSpriteHandler::defaultPalette() const
{
    QVector<QRgb> palette(256);
    const quint8* pal = quakePalette();
    for (int i=0; i<palette.size(); ++i)
        palette[i] = qRgba(pal[i*3+0], pal[i*3+1], pal[i*3+2], 0xFF);
    return palette;
}

bool QuakeSpriteHandler::canLoad(QIODevice *device) const
{
    return canLoadHelper(device, IDSPRITEHEADER, SPRITE_QUAKE_VERSION);
}

bool QuakeSpriteHandler::canLoadHelper(QIODevice *device, int id, int ver) const
{
    if (device && device->isReadable())
    {
        int ident;
        if (readStruct(device, ident))
        {
            ident = qFromLittleEndian(ident);
            if (ident == id)
            {
                int version;
                if (readStruct(device, version))
                {
                    version = qFromLittleEndian(version);
                    if (version == ver)
                        return true;
                }
            }
        }
    }
    return false;
}

Sprite QuakeSpriteHandler::load(QIODevice *device, const QString &fileName) const
{
    Sprite sprite;
    dquakesprite_t header;
    if (!readStruct(device, header))
    {
        sprite.setErrorString(QCoreApplication::translate("Sprite", "Could not read sprite header"));
        return sprite;
    }
    makeHostEndians(header);

    QString error;
    QVector<QRgb> palette = readPalette(device, 0, error);
    QVector<SpriteFrame> frames = loadFrames(device, header.numframes, palette, error);
    if (!error.isEmpty())
    {
        sprite.setErrorString(error);
        return sprite;
    }

    QuakeSpriteInfo spriteInfo;
    spriteInfo.size = QSize(header.width, header.height);
    spriteInfo.faceType = header.type;
    spriteInfo.boundingRadius = header.boundingradius;

    QVariant info = QVariant::fromValue(spriteInfo);

    sprite.set(this, info, frames, fileName);
    return sprite;
}

QString QuakeSpriteHandler::write(const Sprite &sprite, QIODevice *device, const QString &fileName) const
{
    return writeImpl(sprite, device, fileName, SPRITE_QUAKE_VERSION);
}

QString QuakeSpriteHandler::writeImpl(const Sprite &sprite, QIODevice *device, const QString &, int version) const
{
    QuakeSpriteInfo info = sprite.info().value<QuakeSpriteInfo>();

    dquakesprite_t header;
    header.ident = IDSPRITEHEADER;
    header.version = version;
    header.type = info.faceType;
    header.boundingradius = info.boundingRadius;
    header.width = info.size.width();
    header.height = info.size.height();
    header.numframes = sprite.frameCount();
    header.beamlength = 0;
    header.synctype = ST_SYNC;

    makeLittleEndians(header);

    if (!writeStruct(device, header))
        return QCoreApplication::translate("Sprite", "Could not write sprite header");

    return writeFrames(device, sprite.frames());
}

QString QuakeSpriteHandler::writeFrames(QIODevice *device, const QVector<SpriteFrame> &frames) const
{
    for (int i=0; i<frames.count(); ++i)
    {
        SpriteFrame spriteFrame = frames.at(i);

        dspriteframetype_t frametype;
        frametype.type = SPR_SINGLE;
        makeLittleEndians(frametype);
        if (!writeStruct(device, frametype))
            return QCoreApplication::translate("Sprite", "Could not write frametype");

        QPoint origin = spriteFrame.info.toPoint();

        dspriteframe_t frameStruct;
        frameStruct.origin[0] = origin.x();
        frameStruct.origin[1] = origin.y();
        frameStruct.width = spriteFrame.image.size().width();
        frameStruct.height = spriteFrame.image.size().height();
        makeLittleEndians(frameStruct);

        if (!writeStruct(device, frameStruct))
            return QCoreApplication::translate("Sprite", "Could not write frame info");

        QString error = writeFrameImage(device, spriteFrame.image);
        if (!error.isEmpty())
            return error;
    }
    return QString();
}

QString QuakeSpriteHandler::writeFrameImage(QIODevice *device, const QImage &image) const
{
    const uchar* imageData = image.constBits();
    if (!writeBytes(device, imageData, image.width()*image.height()))
        return QCoreApplication::translate("Sprite", "Could not write frame data");
    return QString();
}

QString QuakeSpriteHandler::spriteTypeName(int type)
{
    QString name;
    switch (type) {
    case SPR_VP_PARALLEL_UPRIGHT:
        name = "SPR_VP_PARALLEL_UPRIGHT";
        break;
    case SPR_FACING_UPRIGHT:
        name = "SPR_FACING_UPRIGHT";
        break;
    case SPR_VP_PARALLEL:
        name = "SPR_VP_PARALLEL";
        break;
    case SPR_ORIENTED:
        name = "SPR_ORIENTED";
        break;
    case SPR_VP_PARALLEL_ORIENTED:
        name = "SPR_VP_PARALLEL_ORIENTED";
        break;
    case SPR_LABEL:
        name = "SPR_LABEL";
        break;
    case SPR_LABEL_SCALE:
        name = "SPR_LABEL_SCALE";
        break;
    case SPR_OVERHEAD:
        name = "SPR_OVERHEAD";
        break;
    default:
        name = "Unknown";
        break;
    }
    return name;
}

QStringList QuakeSpriteHandler::stringSpriteInfo(const Sprite &sprite) const
{
    QuakeSpriteInfo info = sprite.info().value<QuakeSpriteInfo>();

    QStringList infoList;
    infoList << QCoreApplication::translate("spriteInfo", "Version: %1").arg(formatName());
    infoList << QCoreApplication::translate("spriteInfo", "Type: %1").arg(spriteTypeName(info.faceType));
    infoList << QCoreApplication::translate("spriteInfo", "Bounding radius: %1").arg(QString::number(info.boundingRadius));
    infoList << QCoreApplication::translate("spriteInfo", "Size: %1x%2").arg(info.size.width()).arg(info.size.height());
    infoList << QCoreApplication::translate("spriteInfo", "Total frames: %1").arg(sprite.frameCount());

    return infoList;
}

QStringList QuakeSpriteHandler::stringFrameInfo(const SpriteFrame &frame) const
{
    QPoint origin = frame.info.toPoint();

    QStringList infoList;
    infoList << QCoreApplication::translate("spriteInfo", "Origin: (%1, %2)").arg(origin.x()).arg(origin.y());
    infoList << QCoreApplication::translate("spriteInfo", "Size: %1 x %2").arg(frame.image.width()).arg(frame.image.height());
    return infoList;
}

QVariant QuakeSpriteHandler::generateFrameInfo(const QImage &image) const
{
    return QPoint(-image.width()/2, image.height()/2);
}

QVariant QuakeSpriteHandler::generateSpriteInfo(const QVector<SpriteFrame> &frames) const
{
    QuakeSpriteInfo info;

    if (!frames.isEmpty())
    {
        info.size = frames.first().image.size();
        info.boundingRadius = sqrt(static_cast<float>(info.size.width()*info.size.width() + info.size.height()*info.size.height()))/2;
    }

    return QVariant::fromValue(info);
}

QIcon QuakeSpriteHandler::icon() const
{
    return QIcon(":/images/quake.png");
}

int QuakeSpriteHandler::requiredPaletteSize() const
{
    return 256;
}

QImage QuakeSpriteHandler::convertImage(const QImage &image, const QVector<QRgb> &palette) const
{
    return image.convertToFormat(QImage::Format_Indexed8, palette);
}

QPixmap QuakeSpriteHandler::makeTransparent(const QImage &image, const QColor &background, const QVariant &spriteInfo) const
{
    Q_UNUSED(spriteInfo)
    Q_UNUSED(background)
    QImage myImage = image;
    myImage.setColor(255, qRgba(0,0,0,0));
    return QPixmap::fromImage(myImage);
}

QVector<QRgb> QuakeSpriteHandler::readPalette(QIODevice* device, int texFormat, QString &error) const
{
    Q_UNUSED(device)
    Q_UNUSED(texFormat)
    Q_UNUSED(error)

    return defaultPalette();
}

QVector<SpriteFrame> QuakeSpriteHandler::loadFrames(QIODevice* device, int numframes, const QVector<QRgb>& palette, QString& error) const
{
    error.clear();
    QVector<SpriteFrame> empty;
    QVector<SpriteFrame> frames;

    for (int i=0; i<numframes; ++i)
    {
        dspriteframetype_t frametype;
        if (!readStruct(device, frametype))
        {
            error = QCoreApplication::translate("Sprite", "Could not read frametype");
            return empty;
        }
        makeHostEndians(frametype);

        int groupnumber = 0;
        if (frametype.type == SPR_SINGLE)
        {
            groupnumber = 1;
        }
        else if (frametype.type == SPR_GROUP)
        {
            dspritegroup_t groupStruct;
            if (!readStruct(device, groupStruct))
            {
                error = QCoreApplication::translate("Sprite", "Could not read group info");
                return empty;
            }
            groupStruct.numframes = qToLittleEndian(groupStruct.numframes);

            QVector<dspriteinterval_t> intervals(groupStruct.numframes);
            if (!readStructs(device, intervals.data(), intervals.size()))
            {
                error = QCoreApplication::translate("Sprite", "Could not read intervals");
                return empty;
            }

            groupnumber = groupStruct.numframes;
        }

        for (int j=0; j<groupnumber; ++j)
        {
            dspriteframe_t frameStruct;
            if (!readStruct(device, frameStruct))
            {
                error = QCoreApplication::translate("Sprite", "Could not read frame info");
                return empty;
            }
            makeHostEndians(frameStruct);

            QImage image = loadFrameImage(device, frameStruct.width, frameStruct.height, palette, error);
            if (!error.isEmpty())
            {
                return empty;
            }

            SpriteFrame frame;
            frame.image = image;
            frame.info = QPoint(frameStruct.origin[0], frameStruct.origin[1]);

            frames.append(frame);
        }
    }

    return frames;
}

QImage QuakeSpriteHandler::loadFrameImage(QIODevice *device, int width, int height, const QVector<QRgb> &palette, QString &error) const
{
    error.clear();
    QByteArray dataArr = device->read(width*height);
    if (dataArr.size() != width*height)
    {
        error = QCoreApplication::translate("Sprite", "Could not read frame data");
        return QImage();
    }

    const uchar* dataBytes = reinterpret_cast<const uchar*>(dataArr.constData());
    QImage image(width, height, QImage::Format_Indexed8);
    image.setColorTable(palette);

    uchar* imageData = image.bits();
    for (int j=0; j<width*height; ++j)
        imageData[j] = dataBytes[j];
    return image;
}

QDataStream &operator<<(QDataStream &out, const QuakeSpriteInfo &myObj)
{
    out << myObj.size;
    out << myObj.faceType;
    out << myObj.boundingRadius;
    return out;
}

QDataStream &operator>>(QDataStream &in, QuakeSpriteInfo &myObj)
{
    in >> myObj.size;
    in >> myObj.faceType;
    in >> myObj.boundingRadius;
    return in;
}
