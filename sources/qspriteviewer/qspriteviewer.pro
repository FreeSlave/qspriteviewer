
QT  += core gui

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += widgets
    QTPLUGIN += tga
    QTPLUGIN += pcx
    CONFIG(debug, debug|release) {
        QTDIRNAME = qt5_debug
    } else {
        QTDIRNAME = qt5
    }
} else {
    CONFIG(debug, debug|release) {
        QTDIRNAME = qt4_debug
    } else {
        QTDIRNAME = qt4
    }
}

win32 {
    RC_FILE = qspriteviewer.rc
}

TEMPLATE = app

linux-g++ {
    QMAKE_CXXFLAGS_RELEASE += -static-libgcc -static-libstdc++
    QMAKE_LFLAGS_RELEASE += -static-libgcc -static-libstdc++
}

SOURCES += main.cpp\
    sprite.cpp \
    util.cpp \
    spriteviewer.cpp \
    commandline.cpp \
    spritelabel.cpp \
    exportsprite.cpp \
    exportdialog.cpp \
    spriteframelist.cpp \
    palettewidget.cpp \
    exportpalette.cpp \
    paletteviewer.cpp \
    spritecreator.cpp \
    hlspritehandler.cpp \
    spritegn.cpp \
    quakespritehandler.cpp \
    quake2spritehandler.cpp \
    spriteloader.cpp \
    pakdialog.cpp \
    pakfile.cpp \
    sprite32handler.cpp \
    microsoftpalettehandler.cpp \
    jascpalettehandler.cpp \
    plainpalettehandler.cpp \
    photoshopswatchhandler.cpp \
    paletteloader.cpp

HEADERS  += \
    sprite.h \
    spritegn.h \
    util.h \
    spriteviewer.h \
    commandline.h \
    spritelabel.h \
    exportsprite.h \
    exportdialog.h \
    spriteframelist.h \
    palettewidget.h \
    exportpalette.h \
    paletteviewer.h \
    spritecreator.h \
    spritehandler.h \
    hlspritehandler.h \
    quakespritehandler.h \
    quake2spritehandler.h \
    spriteloader.h \
    pakdialog.h \
    pakfile.h \
    sprite32handler.h \
    palettehandler.h \
    microsoftpalettehandler.h \
    jascpalettehandler.h \
    plainpalettehandler.h \
    photoshopswatchhandler.h \
    paletteloader.h

TRANSLATIONS = translations/qspriteviewer.ru.ts

TARGET = qspriteviewer

REPODIR = $$PWD/../..

OBJECTS_DIR = $$REPODIR/intermediate/$$QTDIRNAME/$$TARGET
MOC_DIR = $$OBJECTS_DIR
RCC_DIR = $$OBJECTS_DIR
DESTDIR = $$REPODIR/build/$$QTDIRNAME/QSpriteViewer

unix {
    QMAKE_POST_LINK += -$(COPY_FILE) images/qspriteviewer.png $$DESTDIR; $(COPY_FILE) scripts/generatedesktop.sh $$DESTDIR
}

LIBS += -L$$REPODIR/build/$$QTDIRNAME/plugins -ltga -lpcx

RESOURCES += \
    resources.qrc

