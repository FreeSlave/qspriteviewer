#ifndef PALETTELOADER_H
#define PALETTELOADER_H

#include "palettehandler.h"
#include <QList>

class PaletteLoader
{
private:
    static QList<const PaletteHandler*> _handlers;
public:
    static void addHandler(const PaletteHandler* handler);
    static QVector<QRgb> load(QIODevice* device);
    static QList<const PaletteHandler*> handlers();
    static const PaletteHandler* handlerByName(const QString &name);
};

#endif // PALETTELOADER_H
