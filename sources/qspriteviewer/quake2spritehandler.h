/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef QUAKE2SPRITEHANDLER_H
#define QUAKE2SPRITEHANDLER_H

#include <QMetaType>
#include "spritehandler.h"

class Quake2SpriteHandler : public SpriteHandler
{
public:
    Quake2SpriteHandler();

    QString formatName() const;
    QString extension() const;
    QString shortIdentifier() const;
    bool isPaletteRequired() const;
    QVector<QRgb> defaultPalette() const;
    bool canLoad(QIODevice* device) const;
    Sprite load(QIODevice* device, const QString& fileName = QString()) const;
    QString write(const Sprite& sprite, QIODevice* device, const QString& fileName = QString()) const;
    QStringList stringSpriteInfo(const Sprite& sprite) const;
    QStringList stringFrameInfo(const SpriteFrame& frame) const;
    QVariant generateFrameInfo(const QImage& image) const;
    QVariant generateSpriteInfo(const QVector<SpriteFrame>& frames) const;
    QIcon icon() const;
    int requiredPaletteSize() const;
    QImage convertImage(const QImage& image, const QVector<QRgb>& palette) const;
    QPixmap makeTransparent(const QImage& image, const QColor& background, const QVariant& spriteInfo) const;
};

struct Quake2SpriteFrameInfo
{
    QPoint origin;
    QString name;
};

QDataStream &operator<<(QDataStream &out, const Quake2SpriteFrameInfo &myObj);
QDataStream &operator>>(QDataStream &in, Quake2SpriteFrameInfo &myObj);

Q_DECLARE_METATYPE(Quake2SpriteFrameInfo)

#endif // QUAKE2SPRITEHANDLER_H
