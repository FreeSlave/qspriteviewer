/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef SPRITECREATOR_H
#define SPRITECREATOR_H

#include <QWizard>
#include <QWizardPage>
#include <QVector>
#include <QColor>
#include <QMap>
#include <QMenu>
#include <QContextMenuEvent>
#include <QListWidget>

#include "paletteviewer.h"
#include "spritelabel.h"

class StartPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit StartPage(QWidget* parent = 0);
    int nextId() const;
};

class PalettePage : public QWizardPage
{
    Q_OBJECT
public:
    explicit PalettePage(QWidget* parent = 0);
    
    virtual void cleanupPage();
    virtual void initializePage();

    QVector<QRgb> colorTable() const;
    
public slots:
    void loadPaletteDialog();
    void loadImageDialog();
    
private:
    void setColorTable(const QVector<QRgb>& colorTable);
    void setDefaultColorTable();
    
    PaletteViewer* _paletteViewer;
};

class ImagesPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit ImagesPage(QWidget* parent = 0);
    ~ImagesPage();

    virtual void cleanupPage();
    virtual void initializePage();

    void clearAll();

    bool validatePage();

    QVector<SpriteFrame> frames() const;

public slots:
    void addImagesDialog();
    void clearAllDialog();
    void removeSelected();

private:
    QListWidget* _listWidget;
};

class SaveSpritePage : public QWizardPage
{
    Q_OBJECT
public:
    explicit SaveSpritePage(QWidget* parent = 0);

    virtual void cleanupPage();
    virtual void initializePage();

    virtual bool validatePage();

public slots:
    void chooseFileName();

private:
    SpriteLabel* _preview;
};

class SpriteCreator : public QWizard
{
    Q_OBJECT
public:

    enum {
        StartPageId = 1,
        PalettePageId,
        ImagesPageId,
        SaveSpritePageId
    };

    explicit SpriteCreator(QWidget *parent = 0);

    QVector<QRgb> colorTable() const;

signals:

public slots:

};

#endif // SPRITECREATOR_H
