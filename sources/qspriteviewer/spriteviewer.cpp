/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include <QAction>
#include <QApplication>
#include <QBuffer>
#include <QCloseEvent>
#include <QDebug>
#include <QDialog>
#include <QDialogButtonBox>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QFont>
#include <QHBoxLayout>
#include <QIcon>
#include <QImageWriter>
#include <QInputDialog>
#include <QMenuBar>
#include <QMessageBox>
#include <QPixmap>
#include <QPushButton>
#include <QSettings>
#include <QSizeF>
#include <QStatusBar>
#include <QTabWidget>
#include <QTextEdit>
#include <QTimerEvent>
#include <QVariant>
#include <QVBoxLayout>
#include <QWheelEvent>

#include "commandline.h"
#include "exportdialog.h"
#include "exportsprite.h"
#include "paletteviewer.h"
#include "pakdialog.h"
#include "pakfile.h"
#include "spritecreator.h"
#include "spriteloader.h"
#include "spriteviewer.h"
#include "util.h"

const int SpriteViewer::maxScaleIndex = 300;
const int SpriteViewer::scaleStep = 20;
const int SpriteViewer::maxHistorySize = 10;

SpriteViewer::SpriteViewer(QWidget *parent)
    : QMainWindow(parent)
{
    setCorner(Qt::TopLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::TopRightCorner, Qt::RightDockWidgetArea);
    setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

    QSettings settings;

    _label = new SpriteLabel();
    _spriteInfo = new QLabel();
    _spriteInfo->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::TextSelectableByKeyboard);
    _spriteInfoBox = new QGroupBox(tr("N/A"));
    QVBoxLayout* infoLayout = new QVBoxLayout;
    infoLayout->addWidget(_spriteInfo);
    _spriteInfoBox->setLayout(infoLayout);
    _spriteInfoDock = new QDockWidget(tr("Sprite information"));
    _spriteInfoDock->setWidget(_spriteInfoBox);
    addDockWidget(Qt::BottomDockWidgetArea, _spriteInfoDock);

    _statusFrame = new QLabel();
    _statusScale = new QLabel();
    _statusFps = new QLabel();
    QStatusBar* status = new QStatusBar;
    status->addWidget(_statusFrame);
    status->addWidget(_statusScale);
    status->addWidget(_statusFps);
    status->setFixedHeight(status->minimumHeight());
    status->setSizeGripEnabled(false);

    setFrameStatus(_label->currentFrameNumber());
    setScaleStatus(_label->scale());
    setFpsStatus(_label->fps());

    connect(_label, SIGNAL(frameUpdated(int)), SLOT(setFrameStatus(int)));
    connect(_label, SIGNAL(fpsChanged(int)), SLOT(setFpsStatus(int)));
    connect(_label, SIGNAL(scaleChanged(int)), SLOT(setScaleStatus(int)));

    QMenu* fileMenu = menuBar()->addMenu(tr("&File"));
    QIcon documentOpenIcon = QIcon::fromTheme("document-open", QIcon(":/images/document-open.png"));
    QAction* openFile = new QAction(documentOpenIcon, tr("Open File..."), this);
    openFile->setShortcut(QKeySequence::Open);

    QAction* openFromPak = new QAction(tr("Open from PAK..."), this);

    _recent = new QMenu(tr("Recent files"));
    updateRecentMenu();

    QIcon exportIcon = QIcon::fromTheme("document-export", QIcon(":/images/document-export.png"));
    QAction* exportSprite = new QAction(exportIcon, tr("Export..."), this);

    QIcon applicationExitIcon = QIcon::fromTheme("application-exit", QIcon(":/images/application-exit.png"));
    QAction* exit = new QAction(applicationExitIcon, tr("Exit"), this);
    exit->setShortcut(QKeySequence(QKeySequence::Close));

    connect(openFile, SIGNAL(triggered()), SLOT(loadSpriteDialog()));
    connect(openFromPak, SIGNAL(triggered()), SLOT(pakDialog()));
    connect(_recent, SIGNAL(triggered(QAction*)), SLOT(loadRecent(QAction*)));
    connect(exportSprite, SIGNAL(triggered()), SLOT(exportSequenceDialog()));
    connect(exit, SIGNAL(triggered()), SLOT(close()));

    fileMenu->addAction(openFile);
    fileMenu->addMenu(_recent);
    fileMenu->addAction(openFromPak);
    fileMenu->addSeparator();
    fileMenu->addAction(exportSprite);
    fileMenu->addSeparator();
    fileMenu->addAction(exit);

    QMenu* viewMenu = menuBar()->addMenu(tr("&View"));

    QIcon animatedIcon = QIcon::fromTheme("media-playback-start", QIcon(":/images/media-playback-start.png"));
    _animated = new QAction(animatedIcon, tr("Start/Pause animation"), this);
    _animated->setShortcut(Qt::Key_Space);
    _animated->setCheckable(true);

    QIcon playOnceIcon = QIcon::fromTheme("media-skip-forward", QIcon(":/images/media-skip-forward.png"));
    _playOnce = new QAction(playOnceIcon, tr("Play once"), this);
    _playOnce->setShortcut(Qt::Key_Backslash);

    QIcon nextFrameIcon = QIcon::fromTheme("media-seek-forward", QIcon(":/images/media-seek-forward.png"));
    _nextFrame = new QAction(nextFrameIcon, tr("Next frame"), this);
    _nextFrame->setShortcut(Qt::Key_Right);

    QIcon prevFrameIcon = QIcon::fromTheme("media-seek-backward", QIcon(":/images/media-seek-backward.png"));
    _prevFrame = new QAction(prevFrameIcon, tr("Prev frame"), this);
    _prevFrame->setShortcut(Qt::Key_Left);

    QIcon zoomInIcon = QIcon::fromTheme("zoom-in", QIcon(":/images/zoom-in.png"));
    QAction* zoomIn = new QAction(zoomInIcon, tr("Zoom In"), this);
    zoomIn->setShortcut(Qt::Key_Equal);

    QIcon zoomOutIcon = QIcon::fromTheme("zoom-out", QIcon(":/images/zoom-out.png"));
    QAction* zoomOut = new QAction(zoomOutIcon, tr("Zoom Out"), this);
    zoomOut->setShortcut(Qt::Key_Minus);

    QIcon zoomOriginalIcon = QIcon::fromTheme("zoom-original", QIcon(":/images/zoom-original.png"));
    QAction* zoomOriginal = new QAction(zoomOriginalIcon,  tr("Actual size"), this);

    QIcon transparentIcon = QIcon(":/images/eye.png");
    QAction* transparentAction = new QAction(transparentIcon, tr("Transparent"), this);
    transparentAction->setCheckable(true);
    transparentAction->setChecked(false);

    _animated->setEnabled(false);
    _playOnce->setEnabled(false);
    _nextFrame->setEnabled(false);
    _prevFrame->setEnabled(false);

    connect(_animated, SIGNAL(toggled(bool)), _nextFrame, SLOT(setDisabled(bool)));
    connect(_animated, SIGNAL(toggled(bool)), _prevFrame, SLOT(setDisabled(bool)));
    connect(_animated, SIGNAL(toggled(bool)), _label, SLOT(setRunning(bool)));
    connect(_playOnce, SIGNAL(triggered()), SLOT(playOnce()));
    connect(_nextFrame, SIGNAL(triggered()), _label, SLOT(nextFrame()));
    connect(_prevFrame, SIGNAL(triggered()), _label, SLOT(prevFrame()));
    connect(zoomIn, SIGNAL(triggered()), SLOT(zoomIn()));
    connect(zoomOut, SIGNAL(triggered()), SLOT(zoomOut()));
    connect(zoomOriginal, SIGNAL(triggered()), SLOT(zoomOriginal()));
    connect(transparentAction, SIGNAL(toggled(bool)), _label, SLOT(setTransparent(bool)));

    _animated->setChecked(true);

    viewMenu->addAction(_animated);
    viewMenu->addAction(_playOnce);
    viewMenu->addAction(_nextFrame);
    viewMenu->addAction(_prevFrame);
    viewMenu->addSeparator();
    viewMenu->addAction(zoomIn);
    viewMenu->addAction(zoomOut);
    viewMenu->addAction(zoomOriginal);
    viewMenu->addSeparator();
    viewMenu->addAction(transparentAction);

    QMenu* tools = menuBar()->addMenu(tr("&Tools"));
    QAction* changeFps = new QAction(tr("Change fps"), this);
    connect(changeFps, SIGNAL(triggered()), SLOT(changeFpsDialog()));

    QIcon viewColorTableIcon = QIcon(QIcon(":/images/palette.png"));
    QAction* viewColorTable = new QAction(viewColorTableIcon, tr("View palette"), this);
    connect(viewColorTable, SIGNAL(triggered()), SLOT(paletteDialog()));

    QIcon spriteCreatorIcon = QIcon::fromTheme("document-new", QIcon(":/images/document-new.png"));
    QAction* spriteCreator = new QAction(spriteCreatorIcon, tr("Sprite creator"), this);
    connect(spriteCreator, SIGNAL(triggered()), SLOT(spriteCreatorWizard()));

    tools->addAction(changeFps);
    tools->addAction(viewColorTable);
    tools->addAction(spriteCreator);

    /*QMenu* windowMenu = menuBar()->addMenu(tr("&Window"));

    QAction* showFrameDock = new QAction(tr("Show sprite frames"), this);
    showFrameDock->setCheckable(true);

    QAction* showSpriteInfo = new QAction(tr("Show sprite info"), this);
    showSpriteInfo->setCheckable(true);

    QAction* showSpriteList = new QAction(tr("Show sprite list"), this);
    showSpriteList->setCheckable(true);

    windowMenu->addAction(showFrameDock);
    windowMenu->addAction(showSpriteInfo);
    windowMenu->addAction(showSpriteList);*/

    QMenu* helpMenu = menuBar()->addMenu(tr("&Help"));
    QAction* commandLineUsage = new QAction(tr("Command line usage"), this);
    connect(commandLineUsage, SIGNAL(triggered()), SLOT(commandLineUsageDialog()));
    QAction* aboutProgram = new QAction(tr("About %1").arg(qApp->applicationName()), this);
    aboutProgram->setIcon(qApp->windowIcon());
    connect(aboutProgram, SIGNAL(triggered()), SLOT(about()));
    QAction* aboutQt = new QAction(tr("About Qt"), this);
    aboutQt->setIcon(QIcon(":/images/qt-logo.png"));
    connect(aboutQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

    helpMenu->addAction(commandLineUsage);
    helpMenu->addSeparator();
    helpMenu->addAction(aboutProgram);
    helpMenu->addAction(aboutQt);

    QToolBar* spriteToolBar = new QToolBar;

    spriteToolBar->addAction(_animated);
    spriteToolBar->addAction(_playOnce);
    spriteToolBar->addAction(_prevFrame);
    spriteToolBar->addAction(_nextFrame);
    spriteToolBar->addAction(zoomIn);
    spriteToolBar->addAction(zoomOut);
    spriteToolBar->addAction(zoomOriginal);
    spriteToolBar->addAction(viewColorTable);
    spriteToolBar->addAction(transparentAction);

    QWidget* centerWidget = new QWidget;
    QVBoxLayout* centerWidgetLayout = new QVBoxLayout;
    centerWidgetLayout->addWidget(_label);
    centerWidgetLayout->addWidget(spriteToolBar);
    centerWidgetLayout->addWidget(status);
    centerWidget->setLayout(centerWidgetLayout);

    setCentralWidget(centerWidget);


    _spriteFileBar = addToolBar(tr("Sprite list"));

    _spritesComboBox = new QComboBox;
    _spritesComboBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    connect(_spritesComboBox, SIGNAL(activated(int)), SLOT(loadSpriteHint(int)));

    _spriteFileBar->addAction(openFile);
    _spriteFileBar->addAction(exportSprite);
    _spriteFileBar->addAction(spriteCreator);
    _spriteFileBar->addSeparator();
    _spriteFileBar->addWidget(_spritesComboBox);

    _frameList = new SpriteFrameList();
    _spriteFrameDock = new QDockWidget(tr("Sprite frames"));
    _spriteFrameDock->setWidget(_frameList);
    addDockWidget(Qt::LeftDockWidgetArea, _spriteFrameDock);
    connect(_frameList, SIGNAL(exportRequested(int,QString)), SLOT(exportFrameDialog(int,QString)));

    QPoint windowPos = settings.value("window_pos", pos()).toPoint();
    QSize windowSize = settings.value("window_size", QSize(288, 256)).toSize();

    /*connect(showFrameDock, SIGNAL(toggled(bool)), _spriteFrameDock, SLOT(setVisible(bool)));
    connect(_spriteFrameDock, SIGNAL(visibilityChanged(bool)), showFrameDock, SLOT(setChecked(bool)));

    connect(showSpriteInfo, SIGNAL(toggled(bool)), _spriteInfoDock, SLOT(setVisible(bool)));
    connect(_spriteInfoDock, SIGNAL(visibilityChanged(bool)), showSpriteInfo, SLOT(setChecked(bool)));

    connect(showSpriteList, SIGNAL(toggled(bool)), _spriteFileBar, SLOT(setVisible(bool)));
    connect(_spriteFileBar, SIGNAL(visibilityChanged(bool)), showSpriteList, SLOT(setChecked(bool)));*/

    _spriteFrameDock->setVisible(settings.value("show_frame_dock", true).toBool());
    _spriteInfoDock->setVisible(settings.value("show_sprite_info", true).toBool());
    _spriteFileBar->setVisible(settings.value("show_sprite_list", true).toBool());

    resize(windowSize);
    move(windowPos);
}

SpriteViewer::~SpriteViewer()
{

}

void SpriteViewer::closeEvent(QCloseEvent *event)
{
    QSettings settings;
    settings.setValue("window_size", size());
    settings.setValue("window_pos", pos());

    settings.setValue("show_sprite_info", _spriteInfoDock->isVisible());
    settings.setValue("show_frame_dock", _spriteFrameDock->isVisible());
    settings.setValue("show_sprite_list", _spriteFileBar->isVisible());

    event->accept();
}

void SpriteViewer::wheelEvent(QWheelEvent *event)
{
#if QT_VERSION < 0x050000
    int delta = event->delta();
#else
    int delta = event->angleDelta().y();
#endif
    if (delta > 0)
        prevSprite();
    else if (delta < 0)
        nextSprite();
    event->accept();
}

void SpriteViewer::prevSprite()
{
    if (_spritesComboBox->currentIndex() > 0)
    {
        _spritesComboBox->setCurrentIndex(_spritesComboBox->currentIndex()-1);
        loadSpriteHint(_spritesComboBox->currentIndex());
    }
}

void SpriteViewer::nextSprite()
{
    if (_spritesComboBox->currentIndex() < _spritesComboBox->count() - 1)
    {
        _spritesComboBox->setCurrentIndex(_spritesComboBox->currentIndex() + 1);
        loadSpriteHint(_spritesComboBox->currentIndex());
    }
}

void SpriteViewer::loadSpriteDialog()
{
    QSettings settings;
    QString path = settings.value("open_directory", QString()).toString();

    QStringList extensions;
    QStringList filters;
    QList<const SpriteHandler*> handlers = SpriteLoader::handlers();
    foreach (const SpriteHandler* handler, handlers) {
        QString extension = handler->extension();
        filters << QString("%1 sprites (*.%2)").arg(handler->formatName(), extension);
        if (!extensions.contains(extension))
            extensions << extension;
    }
    QString filter = QString("Sprites (*.%1);;%2;;Any (*)").arg(extensions.join(" *."), filters.join(";;"));

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"), path, filter);
    if (!fileName.isEmpty())
    {
        loadFromFile(fileName);
        settings.setValue("open_directory", QFileInfo(fileName).path());
    }
}

void SpriteViewer::loadFromFile(const QString &fileName)
{
    Sprite sprite = SpriteLoader::load(fileName);
    if (!sprite.isValid())
    {
        QMessageBox::critical(this, qApp->applicationName(), tr("%1: failed to read sprite: %2").arg(fileName, sprite.errorString()));
    }
    else
    {
        QFileInfo fileInfo(fileName);
        if (!_label->sprite().isValid() || fileInfo.dir() != QFileInfo(_label->sprite().fileName()).dir() || !_pakData.isEmpty())
        {
            QStringList filterList;
            foreach (const SpriteHandler* handler, SpriteLoader::handlers()) {
                QString extension = QString("*.%1").arg(handler->extension());
                if (!filterList.contains(extension))
                    filterList << extension;
            }

            QStringList entryList = fileInfo.dir().entryList(filterList, QDir::Readable | QDir::Files, QDir::Name);
            _spritesComboBox->clear();
            _spritesComboBox->addItems(entryList);

            _pakData.clear();
            _pakFileName.clear();
        }
        int index = _spritesComboBox->findText(QFileInfo(fileName).fileName());
        if (index != -1)
        {
            _spritesComboBox->setCurrentIndex(index);
        }
        addRecent(fileName);
        setWindowTitle(QFileInfo(fileName).absoluteFilePath() + " - " + qApp->applicationName());

        onLoad(sprite);
    }
}

bool SpriteViewer::loadFromPak(QByteArray pakData, const QString &fileName, const QString &name, const QSize &pos)
{
    QBuffer pakBuffer(&pakData);
    pakBuffer.open(QIODevice::ReadOnly);

    if (pakBuffer.seek(pos.height()))
    {
        Sprite sprite = SpriteLoader::load(&pakBuffer);

        if (sprite.isValid())
        {
            setWindowTitle(QString("%1/%2 - %3").arg(QFileInfo(fileName).absoluteFilePath(), name, qApp->applicationName()));
            onLoad(sprite);
            return true;
        }
        else
        {
            QMessageBox::critical(this, qApp->applicationName(), tr("%1/%2: failed to load sprite from pak file: %3").arg(fileName, name, sprite.errorString()));
        }
    }
    else
    {
        QMessageBox::critical(this, qApp->applicationName(), tr("%1: %2: could not seek to position of sprite in the pak file").arg(fileName, name));
    }
    return false;
}

void SpriteViewer::loadSpriteHint(int index)
{
    if (!_pakData.isEmpty())
    {
        loadFromPak(_pakData, _pakFileName, _spritesComboBox->itemText(index), _spritesComboBox->itemData(index, Qt::UserRole).toSize());
    }
    else
    {
        QFileInfo fileInfo(_label->sprite().fileName());
        QDir dir = fileInfo.dir();
        QString filePath = dir.filePath(_spritesComboBox->itemText(index));
        if (filePath != fileInfo.filePath())
            loadFromFile(filePath);
    }
}

void SpriteViewer::pakDialog()
{
    QSettings settings;
    QString path = settings.value("pak_directory", QString()).toString();

    QString fileName = QFileDialog::getOpenFileName(this, qApp->applicationName(), path, "Quake/Half-Life PAK file (*.pak);;Any (*)");

    if (!fileName.isEmpty())
    {
        QFile file(fileName);
        if (file.open(QIODevice::ReadOnly))
        {
            QByteArray data = file.readAll();
            QBuffer buffer(&data);
            buffer.open(QIODevice::ReadOnly);

            QString error;
            QVector<QPair<QString, QSize> > entries = pakSpriteEntries(&buffer, error);

            if (!error.isEmpty())
            {
                QMessageBox::critical(this, qApp->applicationName(), tr("%1: failed to load pak file: %2").arg(fileName, error));
            }
            else if (entries.isEmpty())
            {
                QMessageBox::information(this, qApp->applicationName(), tr("%1: could not find any sprites. Nothing can be loaded").arg(fileName));
            }
            else
            {
                PakDialog dialog(entries, this);
                dialog.setWindowTitle(fileName + " - PakViewer");
                if (dialog.exec() == QDialog::Accepted)
                {
                    QPair<QString,QSize> chosen = dialog.chosen();
                    if (!chosen.first.isEmpty())
                    {
                        if (loadFromPak(data, fileName, chosen.first, chosen.second))
                        {
                            _spritesComboBox->clear();
                            for (int i=0; i<entries.size(); ++i)
                            {
                                QPair<QString, QSize> item = entries[i];
                                _spritesComboBox->addItem(item.first, item.second);

                                if (item.first == chosen.first)
                                    _spritesComboBox->setCurrentIndex(i);
                            }

                            _pakData = data;
                            _pakFileName = fileName;
                        }
                    }
                }
            }
        }
        else
        {
            QMessageBox::critical(this, qApp->applicationName(), tr("%1: failed to open pak file: %2").arg(fileName, file.errorString()));
        }

        settings.setValue("pak_directory", QFileInfo(fileName).path());
    }
}

void SpriteViewer::onLoad(const Sprite &sprite)
{
    _label->setSprite(sprite);
    bool isAnimated = sprite.isAnimated();
    _animated->setEnabled(isAnimated);
    _playOnce->setEnabled(isAnimated);
    _nextFrame->setEnabled(isAnimated);
    _prevFrame->setEnabled(isAnimated);

    updateSpriteInfo();
    _frameList->setSprite(_label->sprite());
}

void SpriteViewer::loadRecent(QAction *action)
{
    QString fileName = action->text();
    loadFromFile(fileName);
}

void SpriteViewer::zoomIn()
{
    if (_label->scale() + scaleStep <= maxScaleIndex)
        _label->setScale(_label->scale() + scaleStep);
}

void SpriteViewer::zoomOut()
{
    if (_label->scale() > scaleStep)
        _label->setScale(_label->scale() - scaleStep);
}

void SpriteViewer::zoomOriginal()
{
    _label->setScale(100);
}

void SpriteViewer::exportFrameDialog(int frame, const QString& extension)
{
    if (!_label->sprite().isValid())
    {
        QMessageBox::critical(this, qApp->applicationName(), _label->sprite().errorString());
        return;
    }
    QSettings settings;
    QString dirPath = settings.value("export_directory", QFileInfo(_label->sprite().fileName()).path()).toString();

    QString fileName = fromPattern(defaultPattern(), QFileInfo(_spritesComboBox->currentText()).baseName(), frame, numberOfDigits(_label->sprite().frameCount()), defaultFillChar(), extension);
    QString filePath = QDir(dirPath).filePath(fileName);

    filePath = QFileDialog::getSaveFileName(this, tr("Save file"), filePath, "*." + extension);

    if (!filePath.isEmpty())
    {
        QString error;
        exportFrame(_label->sprite().frameImage(frame), filePath, extension.toUtf8(), &error);
        if (error.isEmpty())
        {
            settings.setValue("export_directory", QFileInfo(filePath).path());
        }
        else
        {
            QMessageBox::critical(this, qApp->applicationName(), error);
        }
    }
}

void SpriteViewer::exportSequenceDialog()
{
    if (!_label->sprite().isValid())
    {
        QMessageBox::critical(this, qApp->applicationName(), _label->sprite().errorString());
        return;
    }

    QSettings settings;
    QString dirPath = settings.value("export_directory", QFileInfo(_label->sprite().fileName()).path()).toString();

    ExportDialog exportDialog(_label->sprite(), QFileInfo(_spritesComboBox->currentText()).baseName(), this);
    exportDialog.setWindowTitle(tr("Export sprite"));
    exportDialog.setPath(dirPath);

    if (exportDialog.exec() == QDialog::Accepted)
        settings.setValue("export_directory", exportDialog.path());
}

void SpriteViewer::changeFpsDialog()
{
    bool ok;
    int i = QInputDialog::getInt(this, qApp->applicationName(), tr("Choose fps value"), _label->fps(), 1, 30, 1, &ok);
    if (ok)
        _label->setFps(i);
}

void SpriteViewer::about()
{
    QDialog dialog(this);
    dialog.setWindowTitle(tr("About %1").arg(qApp->applicationName()));

    QDialogButtonBox* buttonBox = new QDialogButtonBox;
    buttonBox->addButton(QDialogButtonBox::Ok);
    connect(buttonBox, SIGNAL(accepted()), &dialog, SLOT(close()));

    QTabWidget* tabWidget = new QTabWidget;

    QString versionStr = tr("Version: %1").arg(qApp->applicationVersion());
    QString qtStr = tr("Using Qt version %1").arg(qVersion());

    QLabel* headerLabel = new QLabel;
    headerLabel->setTextFormat(Qt::RichText);
    headerLabel->setText(QString("<img src=':/images/qspriteviewer.png' width='64' height='64' align='left'><h3>%1</h3><b>%2</b><br>%3")
                         .arg(qApp->applicationName(), versionStr, qtStr));

    QLabel* infoLabel = new QLabel;
    infoLabel->setTextFormat(Qt::RichText);
    infoLabel->setOpenExternalLinks(true);
    infoLabel->setText("Half-Life / Quake / Quake 2 sprite viewer<br><a href='https://bitbucket.org/FreeSlave/qspriteviewer'>Source code repository</a>");

    QLabel* authorsLabel = new QLabel;
    infoLabel->setTextFormat(Qt::RichText);
    authorsLabel->setText("<b>QSpriteViewer by FreeSlave</b><br><br>PCX plugin by Nadeem Hasan<br>TGA plugin by Dominik Seichter");

    QLabel* licenseLabel = new QLabel;
    licenseLabel->setOpenExternalLinks(true);
    licenseLabel->setTextFormat(Qt::RichText);
    licenseLabel->setText("QSpriteViewer is under<br><a href='https://www.gnu.org/licenses/gpl-2.0.txt'>GNU General Public License Version 2</a><br>"
                          "pcx and tga plugins are originally under<br><a href='https://www.gnu.org/licenses/lgpl.txt'>GNU Lesser General Public License</a>");

    tabWidget->addTab(infoLabel, tr("About"));
    tabWidget->addTab(authorsLabel, tr("Authors"));
    tabWidget->addTab(licenseLabel, tr("License"));

    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addWidget(headerLabel);
    vbox->addWidget(tabWidget);
    vbox->addWidget(buttonBox);


    dialog.setLayout(vbox);
    dialog.setTabOrder(buttonBox, tabWidget);
    dialog.setMaximumSize(dialog.sizeHint());
    dialog.exec();
}

void SpriteViewer::commandLineUsageDialog()
{
    QMessageBox message(QMessageBox::NoIcon, qApp->applicationName(), helpMessage(), QMessageBox::Ok, this);
    QFont font;
    font.setFamily("Mono");
    message.setFont(font);
    message.exec();
}

void SpriteViewer::updateSpriteInfo()
{
    _spriteInfoBox->setTitle(_spritesComboBox->currentText());
    QStringList infoList = _label->sprite().stringSpriteInfo();
    _spriteInfo->setText(infoList.join("\n\n"));
}

void SpriteViewer::addRecent(const QString &fileName)
{
    QSettings settings;
    QStringList recent = settings.value("recent", QStringList()).toStringList();
    recent.removeAll(fileName);

    recent.prepend(fileName);
    if (recent.size() >= maxHistorySize)
        recent.removeLast();

    settings.setValue("recent", recent);
    updateRecentMenu();
}

void SpriteViewer::setFrameStatus(int frame)
{
    if (_label->sprite().isValid())
        _statusFrame->setText(tr("frame: %1/%2").arg(frame + 1).arg(_label->sprite().frameCount()));
    else
        _statusFrame->setText(tr("frame: N/A"));
}

void SpriteViewer::setScaleStatus(int scale)
{
    _statusScale->setText(tr("scale: %1%").arg(QString::number(scale)));
}

void SpriteViewer::setFpsStatus(int fps)
{
    _statusFps->setText(tr("fps: %1").arg(fps));
}

void SpriteViewer::updateRecentMenu()
{
    _recent->clear();

    QSettings settings;
    QStringList recent = settings.value("recent", QStringList()).toStringList();
    foreach(QString fileName, recent)
    {
        if (QFileInfo(fileName).exists())
            _recent->addAction(fileName);
    }
}

void SpriteViewer::playOnce()
{
    _animated->setChecked(false);
    _animated->setEnabled(false);
    _label->setCurrentFrame(0);
    connect(_label, SIGNAL(finished()), SLOT(stopPlayOnce()));
    _label->setRunning(true);

    disconnect(_playOnce, SIGNAL(triggered()), this, SLOT(playOnce()));
    connect(_playOnce, SIGNAL(triggered()), this, SLOT(stopPlayOnce()));
    _playOnce->setText(tr("Cancel playing once"));
}

void SpriteViewer::stopPlayOnce()
{
    disconnect(_label, SIGNAL(finished()), this, SLOT(stopPlayOnce()));
    disconnect(_playOnce, SIGNAL(triggered()), this, SLOT(stopPlayOnce()));
    connect(_playOnce, SIGNAL(triggered()), SLOT(playOnce()));

    _label->setRunning(false);
    _animated->setEnabled(true);
    _playOnce->setText(tr("Play once"));
}

void SpriteViewer::paletteDialog()
{
    QSettings settings;
    QString dirPath = settings.value("export_directory", QFileInfo(_label->sprite().fileName()).path()).toString();

    QDialog dialog(this);
    dialog.setWindowTitle(tr("Palette view"));

    PaletteViewer* paletteViewer = new PaletteViewer(_label->sprite().colormap(), this);
    paletteViewer->setFilePath(QDir(dirPath).filePath(QFileInfo(_spritesComboBox->currentText()).baseName()));

    QDialogButtonBox* buttonBox = new QDialogButtonBox;
    QPushButton* exportPalette = buttonBox->addButton(tr("Export"), QDialogButtonBox::ActionRole);
    QPushButton* closeCmd = buttonBox->addButton(QDialogButtonBox::Close);

    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addWidget(paletteViewer);
    vbox->addWidget(buttonBox);

    connect(closeCmd, SIGNAL(clicked()), &dialog, SLOT(close()));
    connect(exportPalette, SIGNAL(clicked()), paletteViewer, SLOT(exportPalette()));

    dialog.setLayout(vbox);

    dialog.exec();
}

void SpriteViewer::spriteCreatorWizard()
{
    SpriteCreator creator;
    creator.setWindowTitle(tr("Sprite creator"));
    creator.exec();
}
