/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef EXPORTPALETTE_H
#define EXPORTPALETTE_H

#include <QVector>
#include <QColor>
#include <QString>

#include "palettehandler.h"

bool exportPalette(const QVector<QRgb>& colormap, const QString& fileName, const PaletteHandler *handler, QString* error = 0);

#endif // EXPORTPALETTE_H
