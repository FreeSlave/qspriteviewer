/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include <QCoreApplication>
#include <QtEndian>

#include <cmath>

#include "sprite32handler.h"
#include "spritegn.h"
#include "util.h"

Sprite32Handler::Sprite32Handler()
{
}

QString Sprite32Handler::formatName() const
{
    return "Sprite 32";
}

QString Sprite32Handler::extension() const
{
    return "spr32";
}

QString Sprite32Handler::shortIdentifier() const
{
    return "s32";
}

QVector<QRgb> Sprite32Handler::defaultPalette() const
{
    return QVector<QRgb>();
}

bool Sprite32Handler::canLoad(QIODevice *device) const
{
    return canLoadHelper(device, IDSPRITEHEADER, SPRITE32_VERSION);
}

QString Sprite32Handler::write(const Sprite &sprite, QIODevice *device, const QString &fileName) const
{
    return writeImpl(sprite, device, fileName, SPRITE32_VERSION);
}

QIcon Sprite32Handler::icon() const
{
    return QIcon(":/images/darkplaces.png");
}

int Sprite32Handler::requiredPaletteSize() const
{
    return 0;
}

QImage Sprite32Handler::convertImage(const QImage &image, const QVector<QRgb> &palette) const
{
    Q_UNUSED(palette)
    return image.convertToFormat(QImage::Format_ARGB32);
}

QPixmap Sprite32Handler::makeTransparent(const QImage &image, const QColor &background, const QVariant &spriteInfo) const
{
    Q_UNUSED(spriteInfo)
    Q_UNUSED(background)
    return QPixmap::fromImage(image);
}

QImage Sprite32Handler::loadFrameImage(QIODevice* device, int width, int height, const QVector<QRgb>& palette, QString& error) const
{
    Q_UNUSED(palette)
    error.clear();
    QByteArray dataArr = device->read(width*height*4);
    if (dataArr.size() != width*height*4)
    {
        error = QCoreApplication::translate("Sprite", "Could not read frame data");
        return QImage();
    }

    const uchar* dataBytes = reinterpret_cast<const uchar*>(dataArr.constData());
    QImage image(width, height, QImage::Format_ARGB32);
    uchar* imageData = image.bits();
    for (int j=0; j<height*width; ++j)
    {
        imageData[j*4+3] = dataBytes[j*4+3];
        imageData[j*4+0] = dataBytes[j*4+2];
        imageData[j*4+1] = dataBytes[j*4+1];
        imageData[j*4+2] = dataBytes[j*4+0];
    }
    return image;
}

QString Sprite32Handler::writeFrameImage(QIODevice *device, const QImage &image) const
{
    QByteArray pixelArr(image.width()*image.height()*4, '\0');
    uchar* pixelData = reinterpret_cast<uchar*>(pixelArr.data());
    for (int j=0; j<image.height(); ++j)
    {
        for (int k=0; k<image.width(); ++k)
        {
            QRgb pixel = image.pixel(k, j);
            int b = j*4*image.width()+k*4;
            pixelData[b+0] = qRed(pixel);
            pixelData[b+1] = qGreen(pixel);
            pixelData[b+2] = qBlue(pixel);
            pixelData[b+3] = qAlpha(pixel);
        }
    }
    if (device->write(pixelArr) != pixelArr.size())
        return QCoreApplication::translate("Sprite", "Could not write frame data");
    return QString();
}
