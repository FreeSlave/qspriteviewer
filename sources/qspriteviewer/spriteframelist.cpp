/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include "spriteframelist.h"
#include <QAction>
#include <QApplication>
#include <QIcon>
#include <QImageWriter>
#include <QListWidgetItem>
#include <QPixmap>
#include <QVariant>
#include <QVBoxLayout>

SpriteFrameList::SpriteFrameList(QWidget *parent) :
    QWidget(parent)
{
    _listWidget = new QListWidget;
    _listWidget->setSpacing(4);
    _listWidget->setUniformItemSizes(true);

    QMenu* exportFrame = new QMenu(tr("Export as..."));
    QList<QByteArray> formats = QImageWriter::supportedImageFormats();
    foreach (QByteArray format, formats) {
        QAction* exportAction = new QAction(QString(format), this);
        exportFrame->addAction(exportAction);
    }
    connect(exportFrame, SIGNAL(triggered(QAction*)), SLOT(requestExport(QAction*)));

    _frameMenu = new QMenu(this);
    _frameMenu->addMenu(exportFrame);

    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addWidget(_listWidget);
    setLayout(vbox);
}

void SpriteFrameList::setSprite(const Sprite &sprite)
{
    _listWidget->clear();
    _listWidget->setIconSize(sprite.frameImage(0).size());

    for (int i=0; i<sprite.frameCount(); ++i)
    {
        QListWidgetItem* item = new QListWidgetItem();
        item->setData(Qt::UserRole, QVariant(i));
        QIcon icon(QPixmap::fromImage(sprite.frameImage(i)));
        item->setIcon(icon);

        QStringList infoList = sprite.stringFrameInfo(i);
        QString text = infoList.join("\n");
        item->setText(text);

        _listWidget->addItem(item);
    }
}

void SpriteFrameList::contextMenuEvent(QContextMenuEvent *event)
{
    QWidget::contextMenuEvent(event);
    QPoint p = _listWidget->mapFromParent(event->pos());
    QListWidgetItem* item = _listWidget->itemAt(p);
    if (item)
    {
        _frameMenu->exec(event->globalPos());
    }
}

void SpriteFrameList::requestExport(QAction *action)
{
    QListWidgetItem* item = _listWidget->currentItem();
    if (item)
    {
        int frame = item->data(Qt::UserRole).toInt();
        emit exportRequested(frame, action->text());
    }
}
