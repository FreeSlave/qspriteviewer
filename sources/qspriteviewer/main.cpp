/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QLocale>
#include <QMetaType>
#include <QTranslator>
#include <QIcon>
#include <cstring>
#include <cstdlib>

#include "commandline.h"
#include "spriteviewer.h"
#include "util.h"

#include "hlspritehandler.h"
#include "quakespritehandler.h"
#include "quake2spritehandler.h"
#include "sprite32handler.h"

#include "spriteloader.h"

#include "microsoftpalettehandler.h"
#include "jascpalettehandler.h"
#include "photoshopswatchhandler.h"
#include "plainpalettehandler.h"

#include "paletteloader.h"

#include <QtPlugin>

#if QT_VERSION < 0x050000
Q_IMPORT_PLUGIN(tga)
Q_IMPORT_PLUGIN(pcx)
#else
Q_IMPORT_PLUGIN(TGAPlugin)
Q_IMPORT_PLUGIN(PCXPlugin)
#endif

void initApplication(QCoreApplication* app, QTranslator* translator)
{
    app->setOrganizationName("HazardTeam");
    app->setApplicationName("QSpriteViewer");
    app->setApplicationVersion("1.2.5");

    if (translator->load(QString("qspriteviewer.%1").arg(QLocale::system().name()), optionValue("--translations", "translations")))
    {
        QCoreApplication::installTranslator(translator);
    }
}

#ifdef Q_WS_WIN

#include <windows.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>

static const WORD MAX_CONSOLE_LINES = 500;

#include <windows.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>

void initializeConsole()
{
    int hConHandle;
    long lStdHandle;
    CONSOLE_SCREEN_BUFFER_INFO coninfo;

    FILE *fp;
    AllocConsole();

    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
    coninfo.dwSize.Y = MAX_CONSOLE_LINES;

    SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

    lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
    hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
    fp = _fdopen( hConHandle, "w" );
    *stdout = *fp;
    setvbuf( stdout, NULL, _IONBF, 0 );

    lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
    hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
    fp = _fdopen( hConHandle, "r" );
    *stdin = *fp;
    setvbuf( stdin, NULL, _IONBF, 0 );

    lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
    hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
    fp = _fdopen( hConHandle, "w" );
    *stderr = *fp;
    setvbuf( stderr, NULL, _IONBF, 0 );
}

#endif

int main(int argc, char *argv[])
{
    int console = 0;
    bool help = false;
    for (int i=1; i<argc; ++i)
    {
        if (strcmp(argv[i], "--console") == 0 || strcmp(argv[i], "-console") == 0)
        {
            console = i;
            break;
        }
        else if (strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0)
        {
            help = true;
            break;
        }
    }

    qRegisterMetaType<HLSpriteInfo>("HLSpriteInfo");
    qRegisterMetaTypeStreamOperators<HLSpriteInfo>("HLSpriteInfo");
    qRegisterMetaType<QuakeSpriteInfo>("QuakeSpriteInfo");
    qRegisterMetaTypeStreamOperators<QuakeSpriteInfo>("QuakeSpriteInfo");
    qRegisterMetaType<Quake2SpriteFrameInfo>("Quake2SpriteFrameInfo");
    qRegisterMetaTypeStreamOperators<Quake2SpriteFrameInfo>("Quake2SpriteFrameInfo");

    qRegisterMetaType<SpriteFrame>("SpriteFrame");
    qRegisterMetaTypeStreamOperators<SpriteFrame>("SpriteFrame");

    HLSpriteHandler hlHandler;
    QuakeSpriteHandler quakeHandler;
    Quake2SpriteHandler quake2Handler;
    Sprite32Handler sprite32Handler;

    SpriteLoader::addHandler(&hlHandler);
    SpriteLoader::addHandler(&quakeHandler);
    SpriteLoader::addHandler(&quake2Handler);
    SpriteLoader::addHandler(&sprite32Handler);

    MicrosoftPaletteHandler msHandler;
    JASCPaletteHandler jascHandler;
    PhotoshopSwatchHandler photoshopHandler;
    PlainPaletteHandler plainHandler;

    PaletteLoader::addHandler(&msHandler);
    PaletteLoader::addHandler(&jascHandler);
    PaletteLoader::addHandler(&photoshopHandler);
    PaletteLoader::addHandler(&plainHandler);

    QTranslator translator;

    if (console || help)
    {
        QCoreApplication a(argc, argv);
        initApplication(&a, &translator);
#ifdef Q_WS_WIN
        //initializeConsole();
#endif
        if (help)
        {
            showHelp();
            return EXIT_SUCCESS;
        }

        return commandLine(console);
    }
    else
    {
        QApplication a(argc, argv);

        initApplication(&a, &translator);
        a.setWindowIcon(QIcon(":/images/qspriteviewer.png"));
        SpriteViewer w;
        w.setWindowTitle(a.applicationName());
        w.show();

        const QStringList args = a.arguments();
        for (int i=1; i<args.size(); ++i)
        {
            if (args.at(i).at(0) != '-' && args.at(i-1).at(0) != '-')
            {
                w.loadFromFile(args.at(i));
                break;
            }
        }

        return a.exec();
    }
}
