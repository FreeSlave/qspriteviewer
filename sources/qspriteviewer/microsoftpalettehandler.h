#ifndef MICROSOFTPALETTEHANDLER_H
#define MICROSOFTPALETTEHANDLER_H

#include "palettehandler.h"

class MicrosoftPaletteHandler : public PaletteHandler
{
public:
    MicrosoftPaletteHandler();

    QString name() const;
    QString extension() const;
    bool canLoad(QIODevice* device) const;
    QVector<QRgb> readFromDevice(QIODevice* device) const;
    bool writeToDevice(QIODevice* device, const QVector<QRgb>& pal) const;

    struct Header
    {
        char riff[4];
        qint32 dataSize;
        char type[4];
        char data[4];
        qint32 chunkSize;
        qint16 version;
        qint16 entries;
    };
};

#endif // MICROSOFTPALETTEHANDLER_H
