/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QComboBox>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>

#include "sprite.h"

class ExportDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ExportDialog(const Sprite& sprite, const QString& baseName, QWidget *parent = 0);

    void setPath(const QString& path);
    QString path() const;
    void setBasename(const QString& basename);

signals:

public slots:
    void chooseDirectory();
    void toggleAdvanced();
    void updateExample();
    void validateRange();
    void exportSprite();

private:
    QLineEdit* _pathEdit;
    QComboBox* _formatBox;
    QLineEdit* _basenameEdit;
    QLineEdit* _patternEdit;
    QLineEdit* _fillCharEdit;
    QSpinBox* _fillWidthBox;
    QLineEdit* _rangeEdit;
    QLabel* _rangeErrorLabel;
    QSpinBox* _startWithBox;
    QPushButton* _advancedCmd;
    QLabel* _exampleLabel;
    QWidget* _advancedWidget;

    Sprite _sprite;
};

#endif // EXPORTDIALOG_H
