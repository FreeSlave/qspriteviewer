/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef QUAKESPRITEHANDLER_H
#define QUAKESPRITEHANDLER_H

#include <QMetaType>
#include "spritehandler.h"

const quint8* quakePalette();

class QuakeSpriteHandler : public SpriteHandler
{
public:
    QuakeSpriteHandler();

    QString formatName() const;
    QString extension() const;
    QString shortIdentifier() const;
    bool isPaletteRequired() const;
    QVector<QRgb> defaultPalette() const;
    bool canLoad(QIODevice* device) const;
    Sprite load(QIODevice* device, const QString& fileName = QString()) const;
    QString write(const Sprite& sprite, QIODevice* device, const QString& fileName = QString()) const;
    QStringList stringSpriteInfo(const Sprite& sprite) const;
    QStringList stringFrameInfo(const SpriteFrame& frame) const;
    QVariant generateFrameInfo(const QImage& image) const;
    QVariant generateSpriteInfo(const QVector<SpriteFrame>& frames) const;
    QIcon icon() const;
    int requiredPaletteSize() const;

    QImage convertImage(const QImage& image, const QVector<QRgb>& palette) const;
    QPixmap makeTransparent(const QImage& image, const QColor& background, const QVariant& spriteInfo) const;

    QString writeImpl(const Sprite& sprite, QIODevice* device, const QString& fileName, int version) const;

    virtual QVector<QRgb> readPalette(QIODevice* device, int texFormat, QString& error) const;
    virtual QVector<SpriteFrame> loadFrames(QIODevice* device, int numframes, const QVector<QRgb>& palette, QString& error) const;
    virtual QImage loadFrameImage(QIODevice* device, int width, int height, const QVector<QRgb>& palette, QString& error) const;
    virtual QString writeFrames(QIODevice* device, const QVector<SpriteFrame>& frames) const;
    virtual QString writeFrameImage(QIODevice* device, const QImage& image) const;
    virtual bool canLoadHelper(QIODevice* device, int id, int ver) const;

    static QString spriteTypeName(int type);
};

struct QuakeSpriteInfo
{
    QuakeSpriteInfo();
    QSize size;
    int faceType;
    float boundingRadius;
};

QDataStream &operator<<(QDataStream &out, const QuakeSpriteInfo &myObj);
QDataStream &operator>>(QDataStream &in, QuakeSpriteInfo &myObj);

Q_DECLARE_METATYPE(QuakeSpriteInfo)

#endif // QUAKESPRITEHANDLER_H
