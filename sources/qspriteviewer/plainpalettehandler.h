#ifndef PLAINPALETTEHANDLER_H
#define PLAINPALETTEHANDLER_H

#include "palettehandler.h"

class PlainPaletteHandler : public PaletteHandler
{
public:
    PlainPaletteHandler();

    QString name() const;
    QString extension() const;
    bool canLoad(QIODevice* device) const;
    QVector<QRgb> readFromDevice(QIODevice* device) const;
    bool writeToDevice(QIODevice* device, const QVector<QRgb>& pal) const;
};

#endif // PLAINPALETTEHANDLER_H
