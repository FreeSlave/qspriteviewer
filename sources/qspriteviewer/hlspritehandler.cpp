/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include <QCoreApplication>
#include <QPainter>
#include <QtEndian>

#include <cmath>

#include "hlspritehandler.h"
#include "spritegn.h"
#include "util.h"

HLSpriteInfo::HLSpriteInfo() : texFormat(SPR_ADDITIVE), faceType(SPR_VP_PARALLEL), boundingRadius(0.0f)
{

}

HLSpriteHandler::HLSpriteHandler()
{
}

QString HLSpriteHandler::formatName() const
{
    return "Half-Life";
}

QString HLSpriteHandler::extension() const
{
    return "spr";
}

QString HLSpriteHandler::shortIdentifier() const
{
    return "hl";
}

bool HLSpriteHandler::isPaletteRequired() const
{
    return true;
}

QVector<QRgb> HLSpriteHandler::defaultPalette() const
{
    return QVector<QRgb>();
}

bool HLSpriteHandler::canLoad(QIODevice *device) const
{
    return canLoadHelper(device, IDSPRITEHEADER, SPRITE_HL_VERSION);
}

Sprite HLSpriteHandler::load(QIODevice *device, const QString &fileName) const
{
    Sprite sprite;

    dsprite_t header;
    if (!readStruct(device, header))
    {
        sprite.setErrorString(QCoreApplication::translate("Sprite", "Could not read sprite header"));
        return sprite;
    }
    makeHostEndians(header);

    QString error;
    QVector<QRgb> palette = readPalette(device, header.texFormat, error);
    if (!error.isEmpty())
    {
        sprite.setErrorString(error);
        return sprite;
    }

    QVector<SpriteFrame> frames = loadFrames(device, header.numframes, palette, error);
    if (!error.isEmpty())
    {
        sprite.setErrorString(error);
        return sprite;
    }

    HLSpriteInfo spriteInfo;
    spriteInfo.size = QSize(header.width, header.height);
    spriteInfo.texFormat = header.texFormat;
    spriteInfo.faceType = header.type;
    spriteInfo.boundingRadius = header.boundingradius;

    QVariant info = QVariant::fromValue(spriteInfo);

    sprite.set(this, info, frames, fileName);
    return sprite;
}

QVector<QRgb> HLSpriteHandler::readPalette(QIODevice* device, int texFormat, QString& error) const
{
    error.clear();
    const int mustPalSize = requiredPaletteSize();
    QVector<QRgb> empty;
    short paletteSize;
    if (!readStruct(device, paletteSize))
    {
        error = QCoreApplication::translate("Sprite", "Could not read palette size");
        return empty;
    }
    paletteSize = qFromLittleEndian(paletteSize);
    if (paletteSize != mustPalSize)
    {
        error = QCoreApplication::translate("Sprite", "Unexpected palette size %1 (should be %2)").arg(paletteSize).arg(mustPalSize);
        return empty;
    }

    QByteArray paletteBytes = device->read(paletteSize*3);
    if (paletteBytes.size() != paletteSize*3)
    {
        error = QCoreApplication::translate("Sprite", "Could not read palette");
        return empty;
    }

    QVector<QRgb> palette(mustPalSize);
    const quint8* data = reinterpret_cast<const quint8*>(paletteBytes.constData());
    if (texFormat == SPR_INDEXALPHA)
    {
        for (int i=0; i<mustPalSize; ++i)
            palette[i] = qRgba(data[765], data[766], data[767], i);
    }
    else
    {
        for (int i=0; i<mustPalSize; ++i)
            palette[i] = qRgba(data[i*3+0], data[i*3+1], data[i*3+2], 0xFF);
    }
    return palette;
}

QString HLSpriteHandler::write(const Sprite &sprite, QIODevice *device, const QString &) const
{
    HLSpriteInfo info = sprite.info().value<HLSpriteInfo>();

    dsprite_t header;
    header.ident = IDSPRITEHEADER;
    header.version = SPRITE_HL_VERSION;
    header.type = info.faceType;
    header.texFormat = info.texFormat;
    header.boundingradius = info.boundingRadius;
    header.width = info.size.width();
    header.height = info.size.height();
    header.numframes = sprite.frameCount();
    header.beamlength = 0;
    header.synctype = ST_SYNC;

    makeLittleEndians(header);

    if (!writeStruct(device, header))
        return QCoreApplication::translate("Sprite", "Could not write sprite header");

    const QVector<QRgb> colorTable = sprite.colormap();
    short paletteSize = colorTable.size();
    if (!writeStruct(device, qToLittleEndian(paletteSize)))
        return QCoreApplication::translate("Sprite", "Could not write palette size");

    quint8* palBytes = new quint8[paletteSize*3];
    for (int i=0; i<colorTable.size(); ++i)
    {
        QRgb rgb = colorTable.at(i);
        palBytes[i*3+0] = static_cast<quint8>(qRed(rgb));
        palBytes[i*3+1] = static_cast<quint8>(qGreen(rgb));
        palBytes[i*3+2] = static_cast<quint8>(qBlue(rgb));
    }
    if (!writeBytes(device, palBytes, paletteSize*3))
    {
        delete[]palBytes;
        return QCoreApplication::translate("Sprite", "Could not write palette");
    }
    delete[]palBytes;

    return writeFrames(device, sprite.frames());
}

QString HLSpriteHandler::texFormatName(int texFormat)
{
    QString name;
    switch(texFormat)
    {
    case SPR_ADDITIVE:
        name = "SPR_ADDITIVE";
        break;
    case SPR_NORMAL:
        name = "SPR_NORMAL";
        break;
    case SPR_INDEXALPHA:
        name = "SPR_INDEXALPHA";
        break;
    case SPR_ALPHTEST:
        name = "SPR_ALPHTEST";
        break;
    default:
        name = "Unknown";
    }
    return name;
}

QStringList HLSpriteHandler::stringSpriteInfo(const Sprite &sprite) const
{
    HLSpriteInfo info = sprite.info().value<HLSpriteInfo>();

    QStringList infoList;
    infoList << QCoreApplication::translate("spriteInfo", "Version: %1").arg(formatName());
    infoList << QCoreApplication::translate("spriteInfo", "Type: %1").arg(spriteTypeName(info.faceType));
    infoList << QCoreApplication::translate("spriteInfo", "Texture format: %1").arg(texFormatName(info.texFormat));
    infoList << QCoreApplication::translate("spriteInfo", "Bounding radius: %1").arg(QString::number(info.boundingRadius));
    infoList << QCoreApplication::translate("spriteInfo", "Size: %1x%2").arg(info.size.width()).arg(info.size.height());
    infoList << QCoreApplication::translate("spriteInfo", "Total frames: %1").arg(sprite.frameCount());

    return infoList;
}

QVariant HLSpriteHandler::generateSpriteInfo(const QVector<SpriteFrame> &frames) const
{
    HLSpriteInfo info;

    if (!frames.isEmpty())
    {
        info.size = frames.first().image.size();
        info.boundingRadius = sqrt(static_cast<float>(info.size.width()*info.size.width() + info.size.height()*info.size.height()))/2;
    }

    return QVariant::fromValue(info);
}

QIcon HLSpriteHandler::icon() const
{
    return QIcon(":/images/half-life.tga");
}

QPixmap HLSpriteHandler::makeTransparent(const QImage &image, const QColor &background, const QVariant &spriteInfo) const
{
    HLSpriteInfo info = spriteInfo.value<HLSpriteInfo>();

    switch (info.texFormat) {
    case SPR_ADDITIVE:
    {
        QPixmap pixmap(image.size());
        pixmap.fill(background);
        QPainter painter;

        painter.begin(&pixmap);
        painter.setCompositionMode(QPainter::CompositionMode_Plus);
        painter.drawImage(0,0,image);
        painter.end();
        return pixmap;
    }
        break;
    case SPR_ALPHTEST:
    {
        QImage myImage = image;
        myImage.setColor(255, qRgba(0,0,0,0));
        return QPixmap::fromImage(myImage);
    }
        break;
    default:
        return QPixmap::fromImage(image);
        break;
    }
}

QDataStream &operator<<(QDataStream &out, const HLSpriteInfo &myObj)
{
    out << myObj.size;
    out << myObj.texFormat;
    out << myObj.faceType;
    out << myObj.boundingRadius;
    return out;
}

QDataStream &operator>>(QDataStream &in, HLSpriteInfo &myObj)
{
    in >> myObj.size;
    in >> myObj.texFormat;
    in >> myObj.faceType;
    in >> myObj.boundingRadius;
    return in;
}
