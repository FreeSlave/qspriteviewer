#ifndef PALETTEHANDLER_H
#define PALETTEHANDLER_H

#include <QColor>
#include <QIODevice>
#include <QString>
#include <QVector>

class PaletteHandler
{
public:
    virtual QString name() const = 0;
    virtual QString extension() const = 0;
    virtual bool canLoad(QIODevice* device) const = 0;
    virtual QVector<QRgb> readFromDevice(QIODevice* device) const = 0;
    virtual bool writeToDevice(QIODevice* device, const QVector<QRgb>& pal) const = 0;
};

#endif // PALETTEHANDLER_H
