/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef PAKDIALOG_H
#define PAKDIALOG_H

#include <QDialog>
#include <QPair>
#include <QVector>
#include <QListWidget>

class PakDialog : public QDialog
{
    Q_OBJECT
public:
    explicit PakDialog(const QVector<QPair<QString, QSize> > &entries, QWidget *parent = 0);

    QPair<QString, QSize> chosen() const;

signals:

private slots:
    void slotItemDoubleClicked(QListWidgetItem* item);
    void slotItemClicked(QListWidgetItem* item);

private:
    QListWidget* _listWidget;
    QPair<QString, QSize> _chosen;
};

#endif // PAKDIALOG_H
