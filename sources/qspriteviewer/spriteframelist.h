/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef SPRITEFRAMELIST_H
#define SPRITEFRAMELIST_H

#include <QContextMenuEvent>
#include <QWidget>
#include <QListWidget>
#include <QListView>
#include <QMenu>

#include "sprite.h"

class SpriteFrameList : public QWidget
{
    Q_OBJECT
public:
    explicit SpriteFrameList(QWidget *parent = 0);
    void setSprite(const Sprite& sprite);

protected:
    void contextMenuEvent(QContextMenuEvent* event);

signals:
    void exportRequested(int, const QString&);

public slots:
    void requestExport(QAction* action);

private:
    QListWidget* _listWidget;
    QMenu* _frameMenu;
};

#endif // SPRITEFRAMELIST_H
