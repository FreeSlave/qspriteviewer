/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include "sprite.h"
#include "util.h"

#include <QDir>
#include <QFileInfo>
#include <QFile>
#include <QCoreApplication>

#include "spritehandler.h"

Sprite::Sprite() : _handler(0)
{
    setErrorString(QCoreApplication::translate("Sprite", "Sprite is not loaded"));
}

void Sprite::set(const SpriteHandler *handler, const QVariant &info, const QVector<SpriteFrame> &frames, const QString &fileName)
{
    _errorString.clear();
    _handler = handler;
    _info = info;
    _frames = frames;
    _fileName = fileName;
}

QString Sprite::formatName() const
{
    if (_handler)
        _handler->formatName();
    return "";
}

const SpriteHandler* Sprite::handler() const
{
    return _handler;
}

QString Sprite::errorString() const {
    return _errorString;
}
void Sprite::setErrorString(const QString& str) {
    _errorString = str;
}

bool Sprite::isValid() const {
    return _errorString.isEmpty();
}

QVariant Sprite::info() const {
    return _info;
}

int Sprite::frameCount() const {
    return _frames.size();
}

SpriteFrame Sprite::frame(int i) const {
    if (i < 0 || i >= _frames.size())
        return SpriteFrame();
    return _frames.at(i);
}

QVariant Sprite::frameInfo(int i) const {
    if (i < 0 || i >= _frames.size())
        return QVariant();
    return _frames.at(i).info;
}

QImage Sprite::frameImage(int i) const {
    if (i < 0 || i >= _frames.size())
        return QImage();
    return _frames.at(i).image;
}

QVector<SpriteFrame> Sprite::frames() const {
    return _frames;
}

QString Sprite::fileName() const {
    return _fileName;
}

bool Sprite::isAnimated() const {
    return isValid() && _frames.size() > 1;
}

QVector<QRgb> Sprite::colormap() const
{
    if (frameCount())
    {
        return frameImage(0).colorTable();
    }
    return QVector<QRgb>();
}

QString Sprite::writeToFile(const QString &fileName) const
{
    if (!_handler)
        return QCoreApplication::translate("Sprite", "Invalid sprite");

    QFile file(fileName);
    if (file.open(QIODevice::WriteOnly))
    {
        return _handler->write(*this, &file, fileName);
    }
    return file.errorString();
}

QStringList Sprite::stringSpriteInfo() const
{
    if (_handler && isValid())
        return _handler->stringSpriteInfo(*this);
    return QStringList();
}

QStringList Sprite::stringFrameInfo(int i) const
{
    if (_handler && isValid() && i >= 0 && i < frameCount())
        return _handler->stringFrameInfo(frame(i));
    return QStringList();
}

QDataStream &operator<<(QDataStream &out, const SpriteFrame &myObj)
{
    out << myObj.image << myObj.info;
    return out;
}

QDataStream &operator>>(QDataStream &in, SpriteFrame &myObj)
{
    in >> myObj.image >> myObj.info;
    return in;
}
