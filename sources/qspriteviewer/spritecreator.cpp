/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include <QApplication>
#include <QByteArray>
#include <QComboBox>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QImage>
#include <QImageReader>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QStringList>
#include <QVBoxLayout>

#include "spritecreator.h"
#include "paletteloader.h"
#include "spriteloader.h"

StartPage::StartPage(QWidget *parent) : QWizardPage(parent)
{
    QHBoxLayout* startLayout = new QHBoxLayout;
    QLabel* startLabel = new QLabel(tr("Format: "));
    QComboBox* gameTypes = new QComboBox;
    startLabel->setBuddy(gameTypes);

    startLayout->addWidget(startLabel);
    startLayout->addWidget(gameTypes);
    startLayout->addStretch();
    setLayout(startLayout);

    registerField("game", gameTypes, "currentText", SIGNAL(currentIndexChanged(QString)));

    foreach (const SpriteHandler* handler, SpriteLoader::handlers()) {
        gameTypes->addItem(handler->icon(), handler->formatName());
    }
}

int StartPage::nextId() const
{
    const SpriteHandler* handler = SpriteLoader::handlerByName(field("game").toString());
    if (handler && handler->isPaletteRequired())
        return SpriteCreator::PalettePageId;
    else
        return SpriteCreator::ImagesPageId;
}

PalettePage::PalettePage(QWidget *parent) : QWizardPage(parent)
{

    _paletteViewer = new PaletteViewer(QVector<QRgb>(256));
    _paletteViewer->setEditable(true);
    _paletteViewer->paletteWidget()->setMinimumSize(_paletteViewer->paletteWidget()->sizeHint());

    QPushButton* loadPaletteCmd = new QPushButton(tr("Load palette"));
    connect(loadPaletteCmd, SIGNAL(clicked()), SLOT(loadPaletteDialog()));

    QPushButton* loadImageCmd = new QPushButton(tr("Load palette from sample image"));
    connect(loadImageCmd, SIGNAL(clicked()), SLOT(loadImageDialog()));

    QVBoxLayout* cmdVBox = new QVBoxLayout;
    cmdVBox->addWidget(loadPaletteCmd);
    cmdVBox->addWidget(loadImageCmd);

    QHBoxLayout* hbox = new QHBoxLayout;
    hbox->addLayout(cmdVBox);
    hbox->addStretch();

    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addLayout(hbox);
    vbox->addWidget(_paletteViewer);

    setLayout(vbox);
}

void PalettePage::loadPaletteDialog()
{
    QStringList extensions;
    QStringList filters;
    QList<const PaletteHandler*> handlers = PaletteLoader::handlers();
    foreach (const PaletteHandler* handler, handlers) {
        QString extension = handler->extension();
        filters << QString("%1 (*.%2)").arg(handler->name(), extension);
        if (!extensions.contains(extension))
            extensions << extension;
    }
    QString filter = QString("Palettes (*.%1);;%2;;Any (*)").arg(extensions.join(" *."), filters.join(";;"));

    QString fileName = QFileDialog::getOpenFileName(this, qApp->applicationName(), QDir::homePath(), filter);

    const SpriteHandler* handler = SpriteLoader::handlerByName(field("game").toString());
    const int mustPalSize = handler ? handler->requiredPaletteSize() : 256;

    if (!fileName.isEmpty())
    {
        QFile file(fileName);
        if (file.open(QIODevice::ReadOnly))
        {
            QVector<QRgb> colorTable = PaletteLoader::load(&file);
            if (!colorTable.isEmpty())
            {
                if (colorTable.size() > mustPalSize)
                {
                    QMessageBox::warning(this, qApp->applicationName(), tr("The size of this palette is bigger than %1 and will be truncated.").arg(mustPalSize));
                    colorTable.resize(mustPalSize);
                }
                else if (colorTable.size() < mustPalSize)
                {
                    QMessageBox::warning(this, qApp->applicationName(), tr("The size of this palette is less than %1 and will be complemented with black color").arg(mustPalSize));
                    int oldSize = colorTable.size();
                    colorTable.resize(mustPalSize);
                    for (int i=oldSize; i<colorTable.size(); ++i)
                    {
                        colorTable[i] = qRgb(0,0,0);
                    }
                }
                setColorTable(colorTable);
            }
            else
            {
                QMessageBox::critical(this, qApp->applicationName(), tr("%1: failed to read palette").arg(fileName));
            }
        }
        else
        {
            QMessageBox::critical(this, qApp->applicationName(), file.errorString());
        }
    }
}

void PalettePage::loadImageDialog()
{
    QList<QByteArray> formats = QImageReader::supportedImageFormats();
    QStringList filters;
    filters.reserve(formats.size());
    foreach (QByteArray format, formats) {
        filters.append(QString(format));
    }
    QString filter = QString(tr("Images (*.%1);;Any (*)")).arg(filters.join(" *."));

    QString fileName = QFileDialog::getOpenFileName(this, qApp->applicationName(), QDir::homePath(), filter);
    if (!fileName.isEmpty())
    {
        QImage image(fileName);
        if (!image.isNull())
        {
            if (image.colorCount() == 0)
            {
                if (QMessageBox::question(this, qApp->applicationName(), tr("Image does not have palette. Generate it automatically? (This operation may be lossy)"),
                                          QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes)
                {
                    image = image.convertToFormat(QImage::Format_Indexed8);
                    setColorTable(image.colorTable());
                }
            }
            else
            {
                setColorTable(image.colorTable());
            }
        }
        else
        {
            QMessageBox::critical(this, qApp->applicationName(), tr("%1: failed to load image").arg(fileName));
        }
    }
}

void PalettePage::setColorTable(const QVector<QRgb> &colorTable)
{
    _paletteViewer->paletteWidget()->setColorTable(colorTable);
}

void PalettePage::setDefaultColorTable()
{
    const SpriteHandler* handler = SpriteLoader::handlerByName(field("game").toString());
    const int mustPalSize = handler ? handler->requiredPaletteSize() : 256;
    QVector<QRgb> colorTable(mustPalSize);
    for (int i=0; i<colorTable.size(); ++i)
        colorTable[i] = qRgb(i,i,i);
    _paletteViewer->paletteWidget()->setColorTable(colorTable);
}

void PalettePage::initializePage()
{
    setDefaultColorTable();
}

void PalettePage::cleanupPage()
{
    setDefaultColorTable();
}

QVector<QRgb> PalettePage::colorTable() const
{
    return _paletteViewer->paletteWidget()->colorTable();
}

ImagesPage::ImagesPage(QWidget *parent) : QWizardPage(parent)
{
    QPushButton* addCmd = new QPushButton(tr("Add images"));
    QPushButton* removeCmd = new QPushButton(tr("Delete"));
    removeCmd->setToolTip(tr("Delete selected frames"));
    QPushButton* clearCmd = new QPushButton(tr("Clear all"));

    connect(addCmd, SIGNAL(clicked()), SLOT(addImagesDialog()));
    connect(removeCmd, SIGNAL(clicked()), SLOT(removeSelected()));
    connect(clearCmd, SIGNAL(clicked()), SLOT(clearAllDialog()));

    QHBoxLayout* hbox = new QHBoxLayout;
    hbox->addWidget(addCmd);
    hbox->addWidget(removeCmd);
    hbox->addWidget(clearCmd);
    hbox->addStretch();

    _listWidget = new QListWidget;
    _listWidget->setDragDropMode(QAbstractItemView::InternalMove);
    _listWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);

    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addLayout(hbox);
    vbox->addWidget(_listWidget);
    setLayout(vbox);
}

ImagesPage::~ImagesPage()
{
}

void ImagesPage::addImagesDialog()
{
    QList<QByteArray> formats = QImageReader::supportedImageFormats();
    QStringList filters;
    filters.reserve(formats.size());
    foreach (QByteArray format, formats) {
        filters.append(QString(format));
    }
    QString filter = QString(tr("Images (*.%1);;Any (*)")).arg(filters.join(" *."));

    QStringList fileNames = QFileDialog::getOpenFileNames(this, qApp->applicationName(), QDir::homePath(), filter);

    QVector<QRgb> colorTable = static_cast<SpriteCreator*>(wizard())->colorTable();

    QSize maxSize = QSize(0,0);
    if (_listWidget->count())
    {
        SpriteFrame frame = _listWidget->item(0)->data(Qt::UserRole).value<SpriteFrame>();
        maxSize = frame.image.size();
    }

    const SpriteHandler* handler = SpriteLoader::handlerByName(field("game").toString());

    QList<QImage> images;
    for (int i=0; i<fileNames.size(); ++i)
    {
        QString fileName = fileNames.at(i);
        QImage image(fileName);
        if (image.isNull())
        {
            if (QMessageBox::critical(this, qApp->applicationName(), tr("%1: failed to load image").arg(fileName),
                                      QMessageBox::Ignore | QMessageBox::Abort, QMessageBox::Ignore) == QMessageBox::Abort)
            {
                break;
            }
        }
        else
        {
            image = handler->convertImage(image, colorTable);
            if (image.width() > maxSize.width())
                maxSize.setWidth(image.width());
            if (image.height() > maxSize.height())
                maxSize.setHeight(image.height());

            images.append(image);
        }
    }

    _listWidget->setIconSize(maxSize);
    for (int i=0; i<images.size(); ++i)
    {
        QImage image = images[i];
        QListWidgetItem* item = new QListWidgetItem;
        if (image.size() != maxSize)
            image = image.scaled(maxSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

        SpriteFrame frame;
        frame.image = image;
        if (handler)
            frame.info = handler->generateFrameInfo(image);

        item->setText(handler->stringFrameInfo(frame).join("\n"));
        item->setData(Qt::UserRole, QVariant::fromValue(frame));
        item->setIcon(QPixmap::fromImage(image));
        _listWidget->addItem(item);
    }
}

void ImagesPage::clearAll()
{
    _listWidget->clear();
}

void ImagesPage::clearAllDialog()
{
    if (_listWidget->count() > 0)
    {
        if (QMessageBox::question(this, qApp->applicationName(), tr("Are you sure to delete all frames?"),
                                  QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes)
        {
            clearAll();
        }
    }
}

void ImagesPage::removeSelected()
{
    QList<QListWidgetItem*> selected = _listWidget->selectedItems();
    foreach (QListWidgetItem* item, selected) {
        delete item;
    }
}

void ImagesPage::initializePage()
{
    clearAll();
}

void ImagesPage::cleanupPage()
{
    clearAll();
}

bool ImagesPage::validatePage()
{
    if (_listWidget->count() > 0)
    {
        return true;
    }
    else
    {
        QMessageBox::critical(this, qApp->applicationName(), tr("You should add at least one frame"));
        return false;
    }
}

QVector<SpriteFrame> ImagesPage::frames() const
{
    QVector<SpriteFrame> toReturn(_listWidget->count());
    for (int i=0; i<_listWidget->count(); ++i)
    {
        QListWidgetItem* item = _listWidget->item(i);
        SpriteFrame frame = item->data(Qt::UserRole).value<SpriteFrame>();
        toReturn[i] = frame;
    }
    return toReturn;
}

SaveSpritePage::SaveSpritePage(QWidget *parent) : QWizardPage(parent)
{
    _preview = new SpriteLabel;
    _preview->setRunning(true);

    QLineEdit* pathEdit = new QLineEdit;
    QIcon documentOpenIcon = QIcon::fromTheme("document-open", QIcon(":/images/document-open.png"));
    QPushButton* chooseFile = new QPushButton(documentOpenIcon, "");
    connect(chooseFile, SIGNAL(clicked()), SLOT(chooseFileName()));

    QHBoxLayout* hbox = new QHBoxLayout;
    hbox->addWidget(pathEdit);
    hbox->addWidget(chooseFile);

    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addWidget(_preview);
    vbox->addLayout(hbox);

    setLayout(vbox);

    registerField("saveFileName*", pathEdit);
}

void SaveSpritePage::chooseFileName()
{
    QString game = field("game").toString();

    const SpriteHandler* handler = SpriteLoader::handlerByName(game);

    QString filter = "Any (*)";
    if (handler)
    {
        filter = QString("%1 (*.%2);;Any (*)").arg(handler->formatName(), handler->extension());
    }

    QString fileName = QFileDialog::getSaveFileName(this, qApp->applicationName(), QDir::homePath(), filter);
    if (!fileName.isEmpty())
        setField("saveFileName", fileName);
}

void SaveSpritePage::cleanupPage()
{
    _preview->setSprite(Sprite());
}

void SaveSpritePage::initializePage()
{
    SpriteCreator* creator = static_cast<SpriteCreator*>(wizard());
    ImagesPage* imagesPage = static_cast<ImagesPage*>(creator->page(SpriteCreator::ImagesPageId));
    QVector<SpriteFrame> frames = imagesPage->frames();

    const SpriteHandler* handler = SpriteLoader::handlerByName(field("game").toString());

    Sprite sprite;
    sprite.set(handler, handler->generateSpriteInfo(frames), frames);
    _preview->setSprite(sprite);
}

bool SaveSpritePage::validatePage()
{
    QString fileName = field("saveFileName").toString();

    QString error = _preview->sprite().writeToFile(fileName);

    if (error.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("%1 was successfully saved").arg(fileName));
        return true;
    }
    else
    {
        QMessageBox::critical(this, qApp->applicationName(), tr("%1: failed to save sprite file: %2").arg(fileName, error));
        return false;
    }
}

SpriteCreator::SpriteCreator(QWidget *parent) :
    QWizard(parent)
{
    StartPage* startPage = new StartPage;
    startPage->setTitle(tr("Welcome to Sprite creator"));
    startPage->setSubTitle(tr("This wizard will help you to create sprite file. First choose needed format."));

    PalettePage* palPage = new PalettePage;
    palPage->setTitle(tr("Loading palette"));
    palPage->setSubTitle(tr("Load palette which will be used to create sprite. It should contain 256 colors.<br>"
                   "You can either load palette from <b>.pal</b> file (Microsoft palette format) or load palette from sample image file."));

    ImagesPage* imagesPage = new ImagesPage;
    imagesPage->setTitle(tr("Adding frames"));
    imagesPage->setSubTitle(tr("Add images that will be frames of sprite. "
                               "All images will be automatically scaled to max sized image."));

    SaveSpritePage* savePage = new SaveSpritePage;
    savePage->setTitle(tr("Saving sprite"));
    savePage->setSubTitle(tr("Preview sprite and select path where it should be saved."));

    setSubTitleFormat(Qt::RichText);
    setPage(StartPageId, startPage);
    setPage(PalettePageId, palPage);
    setPage(ImagesPageId, imagesPage);
    setPage(SaveSpritePageId, savePage);
    setStartId(StartPageId);
    setPixmap(QWizard::LogoPixmap, QPixmap(":/images/qspriteviewer.png"));   
}

QVector<QRgb> SpriteCreator::colorTable() const
{
    const SpriteHandler* handler = SpriteLoader::handlerByName(field("game").toString());

    if (!handler)
        return QVector<QRgb>();

    if (handler->isPaletteRequired())
    {
        return static_cast<const PalettePage*>(page(PalettePageId))->colorTable();
    }
    else
    {
        return handler->defaultPalette();
    }
}
