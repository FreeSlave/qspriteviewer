/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include "exportdialog.h"

#include <QApplication>
#include <QDialogButtonBox>
#include <QDir>
#include <QIcon>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QImageWriter>
#include <QMessageBox>
#include <QVariant>
#include <QVBoxLayout>

#include "exportsprite.h"
#include "util.h"

ExportDialog::ExportDialog(const Sprite &sprite, const QString &baseName, QWidget *parent) :
    QDialog(parent), _sprite(sprite)
{
    QLabel* dirLabel = new QLabel;
    _pathEdit = new QLineEdit(QDir::homePath());
    _pathEdit->setToolTip(tr("Path where images will be placed"));

#ifdef Q_WS_X11
    QIcon folderOpenIcon = QIcon::fromTheme("document-open-folder", QIcon(":/images/document-open-folder.png"));
#else
    QIcon folderOpenIcon = style()->standardIcon(QStyle::SP_DirOpenIcon);
#endif
    QPushButton* chooseCmd = new QPushButton(folderOpenIcon, "");
    chooseCmd->setToolTip(tr("Choose output directory"));
    connect(chooseCmd, SIGNAL(clicked()), SLOT(chooseDirectory()));
    dirLabel->setBuddy(_pathEdit);

    QLabel* basenameLabel = new QLabel(tr("Base name: "));
    _basenameEdit = new QLineEdit(baseName);
    _basenameEdit->setToolTip(tr("Base name for files"));
    basenameLabel->setBuddy(_basenameEdit);
    QLabel* formatLabel = new QLabel(tr("Format: "));
    _formatBox = new QComboBox;
    formatLabel->setBuddy(_formatBox);

    QList<QByteArray> formats = QImageWriter::supportedImageFormats();
    foreach (QByteArray format, formats) {
        _formatBox->addItem(QString(format), format);
    }

    QLabel* resultLabel = new QLabel(tr("Result example: "));
    _exampleLabel = new QLabel;

    _advancedCmd = new QPushButton(tr("Advanced >>"));

    QLabel* patternLabel = new QLabel(tr("Pattern: "));
    _patternEdit = new QLineEdit(defaultPattern());
    patternLabel->setBuddy(_patternEdit);

    QLabel* patternHint = new QLabel(tr("%1 is base name\n%2 is number of frame\n%3 is extension"));

    _fillCharEdit = new QLineEdit(QString(defaultFillChar()));
    _fillCharEdit->setMaxLength(1);
    _fillCharEdit->setInputMask("X");
    QLabel* fillLabel = new QLabel(tr("Fill char: "));
    fillLabel->setBuddy(_fillCharEdit);

    _fillWidthBox = new QSpinBox;
    _fillWidthBox->setRange(0, 10);
    _fillWidthBox->setSpecialValueText(tr("Auto"));
    QLabel* fillWidthLabel = new QLabel(tr("Fill width: "));
    fillWidthLabel->setBuddy(_fillWidthBox);

    QLabel* rangeLabel = new QLabel(tr("Frames: "));
    _rangeEdit = new QLineEdit("all");
    _rangeErrorLabel = new QLabel;
    _rangeErrorLabel->setStyleSheet("QLabel { color : red; }");
    rangeLabel->setBuddy(rangeLabel);

    QDialogButtonBox* buttonBox = new QDialogButtonBox;

    QPushButton* exportCmd = buttonBox->addButton(tr("Export"), QDialogButtonBox::ActionRole);
    buttonBox->addButton(QDialogButtonBox::Cancel);


    QHBoxLayout* dirHBox = new QHBoxLayout;
    dirHBox->addWidget(dirLabel);
    dirHBox->addWidget(_pathEdit);
    dirHBox->addWidget(chooseCmd);

    QHBoxLayout* baseHBox = new QHBoxLayout;
    baseHBox->addWidget(basenameLabel);
    baseHBox->addWidget(_basenameEdit);
    baseHBox->addWidget(formatLabel);
    baseHBox->addWidget(_formatBox);

    QHBoxLayout* resultHBox = new QHBoxLayout;
    resultHBox->addWidget(resultLabel);
    resultHBox->addWidget(_exampleLabel);
    resultHBox->addStretch();

    QHBoxLayout* patternHBox = new QHBoxLayout;
    patternHBox->addWidget(patternLabel);
    patternHBox->addWidget(_patternEdit);
    patternHBox->addWidget(patternHint);
    patternHBox->addStretch(1);

    QHBoxLayout* fillHBox = new QHBoxLayout;
    fillHBox->addWidget(fillLabel);
    fillHBox->addWidget(_fillCharEdit);
    fillHBox->addWidget(fillWidthLabel);
    fillHBox->addWidget(_fillWidthBox);
    fillHBox->addStretch(1);

    QHBoxLayout* rangeHBox = new QHBoxLayout;
    rangeHBox->addWidget(rangeLabel);
    rangeHBox->addWidget(_rangeEdit);
    rangeHBox->addWidget(_rangeErrorLabel);
    rangeHBox->addStretch(1);

    _advancedWidget = new QWidget;
    QVBoxLayout* widgetLayout = new QVBoxLayout;
    widgetLayout->addLayout(patternHBox);
    widgetLayout->addLayout(fillHBox);
    widgetLayout->addLayout(rangeHBox);
    _advancedWidget->setLayout(widgetLayout);

    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addLayout(dirHBox);
    vbox->addLayout(baseHBox);
    vbox->addLayout(resultHBox);
    vbox->addWidget(_advancedCmd, 0, Qt::AlignLeft);
    vbox->addWidget(_advancedWidget);
    vbox->addStretch();
    vbox->addWidget(buttonBox);
    setLayout(vbox);

    connect(_advancedCmd, SIGNAL(clicked()), SLOT(toggleAdvanced()));

    connect(_basenameEdit, SIGNAL(editingFinished()), SLOT(updateExample()));
    connect(_patternEdit, SIGNAL(editingFinished()), SLOT(updateExample()));
    connect(_fillCharEdit, SIGNAL(editingFinished()), SLOT(updateExample()));
    connect(_fillWidthBox, SIGNAL(valueChanged(int)), SLOT(updateExample()));
    connect(_formatBox, SIGNAL(currentIndexChanged(int)), SLOT(updateExample()));
    connect(_rangeEdit, SIGNAL(editingFinished()), SLOT(validateRange()));

    connect(exportCmd, SIGNAL(clicked()), SLOT(exportSprite()));
    connect(buttonBox, SIGNAL(rejected()), SLOT(reject()));

    updateExample();
    _advancedWidget->setVisible(false);
}

void ExportDialog::toggleAdvanced()
{
    _advancedWidget->setVisible(!_advancedWidget->isVisible());
    if (_advancedWidget->isVisible())
        _advancedCmd->setText(tr("Advanced <<"));
    else
        _advancedCmd->setText(tr("Advanced >>"));
    adjustSize();
}

void ExportDialog::chooseDirectory()
{
    QString dirname = QFileDialog::getExistingDirectory(this, tr("Choose directory"), _pathEdit->text());
    if (!dirname.isEmpty())
        _pathEdit->setText(dirname);
}

void ExportDialog::updateExample()
{
    QChar fill = _fillCharEdit->text().isEmpty() ? defaultFillChar() : _fillCharEdit->text().at(0);
    int fillWidth = _fillWidthBox->value() ? _fillWidthBox->value() : numberOfDigits(_sprite.frameCount());
    QString str = fromPattern(_patternEdit->text(), _basenameEdit->text(), 0, fillWidth, fill, _formatBox->currentText());
    _exampleLabel->setText(str);
}

QString ExportDialog::path() const
{
    return _pathEdit->text();
}

void ExportDialog::setPath(const QString &path)
{
    _pathEdit->setText(path);
}

void ExportDialog::setBasename(const QString &basename)
{
    _basenameEdit->setText(basename);
}

void ExportDialog::validateRange()
{
    QString error;
    parseFrameRange(_rangeEdit->text(), _sprite.frameCount(), 1, &error);
    _rangeErrorLabel->setText(error);
}

void ExportDialog::exportSprite()
{
    QString error;
    QVector<int> indices = parseFrameRange(_rangeEdit->text(), _sprite.frameCount(), 1, &error);

    if (!error.isEmpty())
    {
        QMessageBox::critical(this, qApp->applicationName(), error);
        return;
    }

    QString fill = _fillCharEdit->text();
    if (!::exportSprite(_sprite, indices, _pathEdit->text(), _basenameEdit->text(), _patternEdit->text(),
                        _fillWidthBox->value(), fill.isEmpty() ? defaultFillChar() : fill.at(0), _formatBox->itemData(_formatBox->currentIndex()).toByteArray(), &error))
    {
        QMessageBox::critical(this, qApp->applicationName(), error);
    }
    else
    {
        accept();
    }
}
