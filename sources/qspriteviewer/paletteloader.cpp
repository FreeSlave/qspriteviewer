#include "paletteloader.h"

QList<const PaletteHandler*> PaletteLoader::_handlers;

void PaletteLoader::addHandler(const PaletteHandler *handler)
{
    _handlers.append(handler);
}

QVector<QRgb> PaletteLoader::load(QIODevice *device)
{
    for (int i=0; i<_handlers.size(); ++i)
    {
        const PaletteHandler* handler = _handlers.at(i);
        qint64 pos = device->pos();
        bool b = handler->canLoad(device);
        device->seek(pos);
        if (b)
            return handler->readFromDevice(device);
    }
    return QVector<QRgb>();
}

QList<const PaletteHandler*> PaletteLoader::handlers()
{
    return _handlers;
}

const PaletteHandler* PaletteLoader::handlerByName(const QString& name)
{
    for (int i=0; i<_handlers.size(); ++i)
    {
        if (_handlers.at(i)->name() == name)
            return _handlers.at(i);
    }
    return 0;
}
