#! /bin/sh

CURRENT=$(dirname -- $(readlink -f -- $0))
FROMMIMEDIR=$CURRENT/../mime
TOMIMEDIR=$HOME/.local/share/mime

cp -f $FROMMIMEDIR/packages/application-x-sprite.xml $TOMIMEDIR/packages
cp -f $FROMMIMEDIR/packages/application-x-sprite2.xml $TOMIMEDIR/packages
update-mime-database $TOMIMEDIR
