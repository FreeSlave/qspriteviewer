#! /bin/sh
# Should be placed in the folder of executable before run

DESKTOPFILE=qspriteviewer.desktop
APPLICATIONNAME=QSpriteViewer
PROGRAMNAME=qspriteviewer
ICON=qspriteviewer.png
MIMETYPE=application/x-sprite

touch $DESKTOPFILE
CURRENT=$(dirname -- $(readlink -f -- $0))

echo "[Desktop Entry]" > $DESKTOPFILE
echo "Type=Application" >> $DESKTOPFILE
echo "Name=$APPLICATIONNAME" >> $DESKTOPFILE
echo "GenericName=$APPLICATIONNAME" >> $DESKTOPFILE
echo "Comment=Half-Life sprite viewer" >> $DESKTOPFILE
echo "Terminal=false" >> $DESKTOPFILE
echo "Categories=Utility;" >> $DESKTOPFILE
echo "MimeType=$MIMETYPE;" >> $DESKTOPFILE
echo "Exec=$CURRENT/$PROGRAMNAME" >> $DESKTOPFILE
echo "Path=$CURRENT" >> $DESKTOPFILE
echo "Icon=$CURRENT/$ICON" >> $DESKTOPFILE
echo "X-Window-Icon=$CURRENT/$ICON" >> $DESKTOPFILE

desktop-file-validate --no-hints $DESKTOPFILE && desktop-file-install --dir=$HOME/.local/share/applications --mode 755 $DESKTOPFILE --rebuild-mime-info-cache

