/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include "exportsprite.h"

#include <QCoreApplication>
#include <QDir>
#include <QImageWriter>
#include <QSet>
#include <QDebug>

#include "util.h"

QString defaultPattern()
{
    return "%1%2.%3";
}

QChar defaultFillChar()
{
    return '0';
}

QVector<int> parseFrameRange(const QString &frameRange, int frameCount, int first, QString *error)
{
    QSet<int> frameSet;

    QStringList commaSeparated = frameRange.split(',', QString::SkipEmptyParts);
    foreach (QString range, commaSeparated) {
        QStringList dashSeparated = range.split('-');

        bool ok;
        if (dashSeparated.size() == 1)
        {
            QString str = dashSeparated.first().trimmed();
            if (str == "all")
            {
                for (int i=0; i<frameCount; ++i)
                    frameSet.insert(i);
            }
            else if (str == "last")
            {
                frameSet.insert(frameCount-1);
            }
            else
            {
                int i = dashSeparated.first().toInt(&ok);
                if (ok && i >= first && i < frameCount + first)
                {
                    frameSet.insert(i-first);
                }
                else
                {
                    if (error)
                        *error = QCoreApplication::translate("parseFrameList", "Must be number between in range [%1, %2] or 'all', not %3")
                            .arg(QString::number(first), QString::number(frameCount+first-1), str);
                    return QVector<int>();
                }
            }
        }
        else if (dashSeparated.size() == 2)
        {
            QString str1 = dashSeparated.at(0).trimmed();
            QString str2 = dashSeparated.at(1).trimmed();

            int from;
            int to;

            from = str1.toInt(&ok);

            if (!(ok && from >= first && from < frameCount + first))
            {
                if (error)
                    *error = QCoreApplication::translate("parseFrameList", "Must be number between in range [%1, %2], not %3")
                        .arg(QString::number(first), QString::number(frameCount+first-1), str1);
                return QVector<int>();
            }

            if (str2 == "last")
            {
                to = frameCount+first-1;
            }
            else
            {
                to = str2.toInt(&ok);

                if (!(ok && to >= first && to < frameCount + first))
                {
                    if (error)
                        *error = QCoreApplication::translate("parseFrameList", "Must be number between in range [%1, %2] or 'last', not %3")
                            .arg(QString::number(first), QString::number(frameCount+first-1), str2);
                    return QVector<int>();
                }
            }

            if (from > to)
            {
                if (error)
                    *error = QCoreApplication::translate("parseFrameList", "Range %1-%2 is invalid. The second value in range should be equal to the first or bigger")
                        .arg(str1, str2);
                return QVector<int>();
            }

            for (int i=from; i<=to; ++i)
                frameSet.insert(i - first);
        }
        else
        {
            if (error)
                *error = QCoreApplication::translate("parseFrameList", "Invalid range %1. Must be one number or two numbers separated by '-'").arg(range);
            return QVector<int>();
        }
    }

    QVector<int> toReturn;
    foreach (int i, frameSet) {
        toReturn.append(i);
    }

    return toReturn;
}

QString fromPattern(const QString& pattern, const QString& baseName, int i, int fillWidth, QChar fillChar, const QString& extension)
{
    return pattern.arg(baseName).arg(QString::number(i), fillWidth, fillChar).arg(QString(extension));
}

bool exportFrame(const QImage &image, const QString &filePath, const QByteArray &format, QString *error)
{
    QImageWriter writer(filePath, format);
    if (!writer.write(image))
    {
        if (error)
            *error = writer.errorString();
        return false;
    }
    return true;
}

bool exportSprite(const Sprite &sprite, QVector<int> indices, const QString &path, const QString &baseName,
                    const QString &pattern, int fillWidth, QChar fillChar, const QByteArray &format, QString *error)
{
    if (!sprite.isValid())
    {
        if (error)
            *error = QCoreApplication::translate("exportSprite", "Invalid sprite (%1)").arg(sprite.errorString());
        return false;
    }

    QDir dir(path);

    for (int i = 0; i<indices.size(); ++i)
    {
        int n = indices.at(i);

        if (n < 0 || n >= sprite.frameCount())
        {
            if (error)
                *error = QCoreApplication::translate("exportSprite", "Invalid frame number");
            return false;
        }

        QString fileName = fromPattern(pattern, baseName, n, fillWidth ? fillWidth : numberOfDigits(sprite.frameCount()), fillChar, QString(format));
        QString filePath = dir.filePath(fileName);

        if (!exportFrame(sprite.frameImage(n), filePath, format, error))
            return false;
    }
    return true;
}
