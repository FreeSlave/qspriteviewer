/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef PALETTEVIEWER_H
#define PALETTEVIEWER_H

#include <QColor>
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QString>
#include <QVector>

#include "palettewidget.h"

class PaletteViewer : public QWidget
{
    Q_OBJECT
public:
    explicit PaletteViewer(const QVector<QRgb>& colormap, QWidget *parent = 0);
    inline void setFilePath(const QString& filePath) {
        _filePath = filePath;
    }
    inline QString filePath() const {
        return _filePath;
    }

    void setEditable(bool editable);
    bool isEditable() const;

    inline PaletteWidget* paletteWidget() {
        return _palWidget;
    }

signals:

public slots:
    void colorSlot(int index, QRgb rgb);
    void exportPalette();
    void changeCurrentColor();

private:

    static QString colorToText(const QRgb &rgb);
    static QColor colorFromText(const QString &text);

    QString _filePath;
    QLabel* _currentIndexLabel;
    QLineEdit* _colorEdit;

    PaletteWidget* _palWidget;
};

#endif // PALETTEVIEWER_H
