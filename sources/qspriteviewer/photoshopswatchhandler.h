#ifndef PHOTOSHOPSWATCHHANDLER_H
#define PHOTOSHOPSWATCHHANDLER_H

#include "palettehandler.h"

class PhotoshopSwatchHandler : public PaletteHandler
{
public:
    PhotoshopSwatchHandler();

    QString name() const;
    QString extension() const;
    bool canLoad(QIODevice* device) const;
    QVector<QRgb> readFromDevice(QIODevice* device) const;
    bool writeToDevice(QIODevice* device, const QVector<QRgb>& pal) const;
};

#endif // PHOTOSHOPSWATCHHANDLER_H
