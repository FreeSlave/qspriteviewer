/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef SPRITE32HANDLER_H
#define SPRITE32HANDLER_H

#include "quakespritehandler.h"

class Sprite32Handler : public QuakeSpriteHandler
{
public:
    Sprite32Handler();

    QString formatName() const;
    QString extension() const;
    QString shortIdentifier() const;
    QVector<QRgb> defaultPalette() const;
    bool canLoad(QIODevice* device) const;
    QString write(const Sprite &sprite, QIODevice *device, const QString &fileName) const;
    QIcon icon() const;
    int requiredPaletteSize() const;
    QImage convertImage(const QImage& image, const QVector<QRgb>& palette) const;
    QPixmap makeTransparent(const QImage& image, const QColor& background, const QVariant& spriteInfo) const;

    QImage loadFrameImage(QIODevice* device, int width, int height, const QVector<QRgb>& palette, QString& error) const;
    QString writeFrameImage(QIODevice* device, const QImage& image) const;
};

#endif // SPRITE32HANDLER_H
