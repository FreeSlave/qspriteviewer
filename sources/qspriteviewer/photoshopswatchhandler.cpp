#include "photoshopswatchhandler.h"

#include <QDataStream>
#include <cstring>

PhotoshopSwatchHandler::PhotoshopSwatchHandler()
{
}

QString PhotoshopSwatchHandler::name() const
{
    return "Photoshop Swatch";
}

QString PhotoshopSwatchHandler::extension() const
{
    return "aco";
}

bool PhotoshopSwatchHandler::canLoad(QIODevice *device) const
{
    if (device && device->isOpen() && device->isReadable())
    {
        QDataStream stream(device);
        stream.setByteOrder(QDataStream::BigEndian);
        short version;
        stream >> version;

        if (stream.status() == QDataStream::Ok && version == 1)
            return true;
    }
    return false;
}

QVector<QRgb> PhotoshopSwatchHandler::readFromDevice(QIODevice *device) const
{
    QDataStream stream(device);
    stream.setByteOrder(QDataStream::BigEndian);
    short version;
    short count;
    stream >> version >> count;

    if (stream.status() != QDataStream::Ok || version != 1 || count <= 0)
        return QVector<QRgb>();

    QVector<QRgb> pal(count);
    for (int i=0; i<count; ++i)
    {
        unsigned short space, w, x, y, z;
        stream >> space >> w >> x >> y >> z;

        if (stream.status() != QDataStream::Ok)
            return QVector<QRgb>();

        switch(space)
        {
        case 0:
            pal[i] = qRgb(w/256, x/256, y/256);
            break;
        case 1:
            pal[i] = QColor::fromHsv(qRound(w/182.04), qRound(x/655.35), qRound(y/655.35)).rgba();
            break;
        case 2:
            pal[i] = QColor::fromCmyk(qRound(100-w/655.35), qRound(100-x/655.35), qRound(100-y/655.35), qRound(100-z/655.35)).rgba();
            break;
        case 7:
            return QVector<QRgb>(); //Lab color space not implemented
            break;
        case 8:
            pal[i] = qRgb(qRound(w/39.0625), qRound(x/39.0625), qRound(y/39.0625));
            break;
        case 9:
            return QVector<QRgb>(); //Wide CMYK color space not implemented
            break;
        default:
            return QVector<QRgb>(); //unknown color space
            break;
        }
    }
    return pal;
}

bool PhotoshopSwatchHandler::writeToDevice(QIODevice *device, const QVector<QRgb> &pal) const
{
    if (device && device->isOpen() && device->isWritable())
    {
        QDataStream stream(device);
        stream.setByteOrder(QDataStream::BigEndian);

        short version = 1;
        short count = static_cast<short>(pal.size());

        stream << version << count;

        if (stream.status() != QDataStream::Ok)
            return false;

        for (int i=0; i<count; ++i)
        {
            QRgb rgb = pal[i];
            unsigned short space = 0;
            unsigned short red = qRed(rgb)*256;
            unsigned short green = qGreen(rgb)*256;
            unsigned short blue = qBlue(rgb)*256;
            unsigned short z = 0;

            stream << space << red << green << blue << z;

            if (stream.status() != QDataStream::Ok)
                return false;
        }
        return true;
    }
    return false;
}

