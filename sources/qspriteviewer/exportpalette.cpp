/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include "exportpalette.h"

#include <QFile>
#include "paletteloader.h"

bool exportPalette(const QVector<QRgb> &colormap, const QString &fileName, const PaletteHandler* handler, QString *error)
{
    QFile file(fileName);
    if (file.open(QIODevice::WriteOnly))
    {
        if (handler->writeToDevice(&file, colormap))
            return true;
    }
    if (error)
        *error = file.errorString();
    return false;
}
