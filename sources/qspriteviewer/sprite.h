/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef SPRITE_H
#define SPRITE_H

#include <QColor>
#include <QImage>
#include <QMetaType>

#include <QString>
#include <QVariant>
#include <QVector>

class SpriteHandler;

struct SpriteFrame
{
    QImage image;
    QVariant info;
};

QDataStream &operator<<(QDataStream &out, const SpriteFrame &myObj);
QDataStream &operator>>(QDataStream &in, SpriteFrame &myObj);

Q_DECLARE_METATYPE(SpriteFrame)

class Sprite
{
public:
    Sprite();
    void set(const SpriteHandler* handler, const QVariant& info, const QVector<SpriteFrame>& frames, const QString& fileName = QString());

    QString formatName() const;
    const SpriteHandler* handler() const;

    QString errorString() const;
    void setErrorString(const QString& str);
    bool isValid() const;
    QVariant info() const;

    int frameCount() const;
    SpriteFrame frame(int i) const;
    QVariant frameInfo(int i) const;
    QImage frameImage(int i) const;
    QVector<SpriteFrame> frames() const;

    QString fileName() const;

    bool isAnimated() const;

    QVector<QRgb> colormap() const;

    QString writeToFile(const QString& fileName) const;

    QStringList stringSpriteInfo() const;
    QStringList stringFrameInfo(int i) const;

private:
    QVariant _info;
    QVector<SpriteFrame> _frames;
    QString _errorString;
    QString _fileName;
    const SpriteHandler* _handler;
};

#endif // SPRITE_H
