/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include "commandline.h"

#include <QCoreApplication>

#include <QDir>
#include <QFileInfo>
#include <QImageWriter>
#include <QTextStream>

#include <cstdio>
#include <cstdlib>

#include "spriteloader.h"
#include "util.h"
#include "exportpalette.h"
#include "exportsprite.h"
#include "plainpalettehandler.h"
#include "paletteloader.h"

QString optionValue(const QStringList &args, const QString& option, const QString& defaultValue)
{
    int i = args.indexOf(option);
    if (i != -1 && i < args.size()-1)
        return args[i+1];
    return defaultValue;
}

QString optionValue(const QString& option, const QString& defaultValue)
{
    return optionValue(QCoreApplication::arguments(), option, defaultValue);
}

void mainHelp(QTextStream &outstream)
{
    QString program = QCoreApplication::arguments().first();
    outstream << QCoreApplication::translate("commandLine", "Usage: %1 [file]").arg(program) << endl;
    outstream << "\n";
    outstream << QCoreApplication::translate("commandLine", "Console usage:") << endl;
    outstream << "  -h, --help              " << QCoreApplication::translate("commandLine", "Display this help.") << endl;
    outstream << "  --translations <folder>   " << QCoreApplication::translate("commandLine", "Search translation files in given folder") << endl;
    outstream << "  --console <subcommand>  " << QCoreApplication::translate("commandLine", "Enter console mode and run subcommand.") << endl;
    outstream << "\n";
    outstream << QCoreApplication::translate("commandLine", "Subcommands:") << endl;
    outstream << "  create-sprite    " << QCoreApplication::translate("commandLine", "Create sprite from set of images.") << endl;
    outstream << "  export-one       " << QCoreApplication::translate("commandLine", "Export one frame of sprite as image.") << endl;
    outstream << "  export-sequence  " << QCoreApplication::translate("commandLine", "Export frames of sprite as separated images.") << endl;
    outstream << "  export-pal       " << QCoreApplication::translate("commandLine", "Export palette of sprite as Microsoft palette file") << endl;
    outstream << "  info             " << QCoreApplication::translate("commandLine", "Show information about sprite.") << endl;
    outstream << QCoreApplication::translate("commandLine", "Run %1 --console <subcommand> --help to get help about specific subcommand").arg(program) << endl;
}

void showHelp()
{
    QTextStream outstream(stdout, QIODevice::WriteOnly | QIODevice::Text);
    mainHelp(outstream);
}

QString helpMessage()
{
    QString message;
    QTextStream outstream(&message, QIODevice::WriteOnly | QIODevice::Text);
    mainHelp(outstream);
    return message;
}

int commandLine(int consoleArg)
{
    QStringList args = QCoreApplication::arguments();

    QTextStream outstream(stdout, QIODevice::WriteOnly | QIODevice::Text);
    QTextStream errstream(stderr, QIODevice::WriteOnly | QIODevice::Text);
    if (consoleArg+1 >= args.size())
    {
        showHelp();
        return EXIT_FAILURE;
    }

    QStringList subprogramArgs = args;
    subprogramArgs.erase(subprogramArgs.begin(), subprogramArgs.begin()+consoleArg+1);
    QString subprogram = subprogramArgs.first();

    CommandLineParser parser(QString("%1 %2 %3").arg(args.at(0), args.at(consoleArg), subprogram));

    if (subprogram == "create-sprite")
    {
        parser.addPositionalArgument("files...", QCoreApplication::translate("commandLine", "Image files"));

        QList<const SpriteHandler*> handlers = SpriteLoader::handlers();
        QStringList idList;
        foreach (const SpriteHandler* handler, handlers) {
            idList.append(handler->shortIdentifier());
        }

        CommandLineOption outputOption("output", QCoreApplication::translate("commandLine", "Write file to output path. By default current directory will be used."), "path");
        CommandLineOption gameOption("game", QCoreApplication::translate("commandLine", "Set format of sprite. Possible values: %1").arg(idList.join(", ")), "type");
        CommandLineOption paletteOption("palette", QCoreApplication::translate("commandLine", "Load palette from palette file or from sample image file."
                                                                         "Makes sense only for those sprites that require palette."),
                                  "palette-file");

        parser.addOption(outputOption);
        parser.addOption(gameOption);
        parser.addOption(paletteOption);

        if (!parser.parse(subprogramArgs))
        {
            errstream << parser.errorString() << endl;
            return EXIT_FAILURE;
        }

        QStringList positionalArgumens = parser.positionalArguments();

        if (positionalArgumens.isEmpty())
        {
            errstream << QCoreApplication::translate("commandLine", "Expected at least one input file") << endl;
            return EXIT_FAILURE;
        }

        QString filePath = parser.value(outputOption);
        QString game = parser.value(gameOption).toLower();

        QString palette = parser.value(paletteOption);

        const SpriteHandler* handler = SpriteLoader::handlerByShortIdentifier(game);

        if (!handler)
        {
            errstream << QCoreApplication::translate("commandLine", "Unknown game type %1").arg(parser.value(gameOption)) << endl;
            return EXIT_FAILURE;
        }

        QFileInfo palFileInfo(palette);
        QVector<QRgb> colorTable;

        QStringList palExtensions;
        foreach (const PaletteHandler* handler, PaletteLoader::handlers()) {
            QString extension = handler->extension();
            if (!palExtensions.contains(extension))
                palExtensions << extension;
        }

        if (handler->isPaletteRequired())
        {
            if (palExtensions.contains(palFileInfo.suffix()))
            {
                QFile file(palette);
                if (file.open(QIODevice::ReadOnly))
                {
                    colorTable = PaletteLoader::load(&file);
                    if (colorTable.isEmpty())
                    {
                        errstream << QCoreApplication::translate("commandLine", "%1: failed to load palette").arg(palette) << endl;
                        return EXIT_FAILURE;
                    }
                    else if (colorTable.size() != handler->requiredPaletteSize())
                    {
                        errstream << QCoreApplication::translate("commandLine", "%1: wrong palette size %2, must be %3")
                                     .arg(palette).arg(colorTable.size()).arg(handler->requiredPaletteSize()) << endl;
                    }
                }
                else
                {
                    errstream << QCoreApplication::translate("commandLine", "%1: failed to load palette").arg(palette) << endl;
                    return EXIT_FAILURE;
                }
            }
            else
            {
                QImage image(palette);
                if (image.isNull())
                {
                    errstream <<QCoreApplication::translate("commandLine", "%1: failed to load palette").arg(palette) << endl;
                    return EXIT_FAILURE;
                }
                else
                {
                    image = image.convertToFormat(QImage::Format_Indexed8);
                    colorTable = image.colorTable();
                }
            }
        }
        else
        {
            colorTable = handler->defaultPalette();
        }

        QSize maxSize = QSize(0,0);
        QVector<QImage> images;
        foreach (QString posArg, positionalArgumens) {
            QImage image(posArg);
            if (image.isNull())
            {
                errstream << QCoreApplication::translate("commandLine", "%1: failed to load image").arg(posArg) << endl;
                return EXIT_FAILURE;
            }
            if (!colorTable.isEmpty())
                image = image.convertToFormat(QImage::Format_Indexed8, colorTable);

            if (image.width() > maxSize.width())
                maxSize.setWidth(image.width());
            if (image.height() > maxSize.height())
                maxSize.setHeight(image.height());

            images.append(image);
        }

        QVector<SpriteFrame> frames;
        foreach (QImage image, images)
        {
            if (image.size() != maxSize)
                image = image.scaled(maxSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

            SpriteFrame frame;
            frame.image = image;
            frame.info = handler->generateFrameInfo(image);

            frames.append(frame);
        }

        Sprite sprite;
        sprite.set(handler, handler->generateSpriteInfo(frames), frames);

        QString error = sprite.writeToFile(filePath);
        if (!error.isEmpty())
        {
            errstream << QCoreApplication::translate("commandLine", "%1: failed to save sprite file: %2").arg(filePath, error) << endl;
            return EXIT_FAILURE;
        }

        return EXIT_SUCCESS;
    }

    else if (subprogram == "export-one")
    {
        parser.addPositionalArgument("file", QCoreApplication::translate("commandLine",
                                                                         "Sprite file"));
        CommandLineOption formatOption("format",
                                        QCoreApplication::translate("commandLine",
                                                                    "Set output format.\n"
                                                                    "If option is not set the format is guessed from output file extension."),
                                        "format",
                                        QString());
        CommandLineOption outputOption("output",
                                        QCoreApplication::translate("commandLine",
                                                                    "Write file to output path. If option contains only directory the file name will be based on input file name and given format.\n"
                                                                    "If option is not set the current directory is used."),
                                        "path",
                                        QString());
        CommandLineOption frameOption("frame",
                                       QCoreApplication::translate("commandLine",
                                                                   "Number of frame to export. Count is starting from 1. By default 1 is used."),
                                       "number",
                                       "1");
        parser.addOption(formatOption);
        parser.addOption(outputOption);
        parser.addOption(frameOption);

        if (!parser.parse(subprogramArgs))
        {
            errstream << parser.errorString() << endl;
            return EXIT_FAILURE;
        }
        QString format = parser.value(formatOption);
        QString output = parser.value(outputOption);
        QString frame = parser.value(frameOption);

        QStringList positionalArgs = parser.positionalArguments();
        if (positionalArgs.size() != 1)
        {
            errstream << QCoreApplication::translate("commandLine", "Exactly one input file expected") << endl;
            return EXIT_FAILURE;
        }

        QString inputFileName = positionalArgs.first();

        Sprite sprite = SpriteLoader::load(inputFileName);
        if (!sprite.isValid())
        {
            errstream << sprite.errorString() << endl;
            return EXIT_FAILURE;
        }

        QFileInfo outputInfo(output);
        QDir directory;
        QString outputFileName;

        if (output.isEmpty())
            directory = QDir::current();
        else if (outputInfo.isDir())
            directory = output;
        else
        {
            directory = outputInfo.path();
            outputFileName = outputInfo.fileName();
        }

        if (format.isEmpty())
        {
            QString suffix = QFileInfo(outputFileName).suffix();
            if (suffix.isEmpty())
            {
                errstream << QCoreApplication::translate("commandLine", "Can't deduce output format. No format nor file extension are given") << endl;
                return EXIT_FAILURE;
            }
            format = suffix;
        }
        else
        {
            if (outputFileName.isEmpty())
            {
                outputFileName = QFileInfo(inputFileName).baseName() + "." + format;
            }
        }

        bool ok;
        int frameNum = frame.toInt(&ok);

        if (!ok || frameNum < 1 || frameNum > sprite.frameCount())
        {
            errstream << QCoreApplication::translate("commandLine", "Invalid frame number") << endl;
            return EXIT_FAILURE;
        }

        QString outputFilePath = directory.filePath(outputFileName);
        QString error;
        if (!exportFrame(sprite.frameImage(frameNum-1), outputFilePath, format.toLocal8Bit(), &error))
        {
            errstream << QCoreApplication::translate("commandLine", "Error occured while writing %1: %2").arg(outputFilePath, error) << endl;
            return EXIT_FAILURE;
        }

        return EXIT_SUCCESS;
    }
    else if (subprogram == "export-sequence")
    {
        parser.addPositionalArgument("file", QCoreApplication::translate("commandLine",
                                                                         "Sprite file"));
        CommandLineOption formatOption("format",
                                        QCoreApplication::translate("commandLine",
                                                                    "Set output format."),
                                        "format");
        CommandLineOption directoryOption("directory",
                                        QCoreApplication::translate("commandLine",
                                                                    "Write files to specified directory. By default the current directory is used."),
                                        "path",
                                        QString());
        CommandLineOption baseNameOption("basename",
                                          QCoreApplication::translate("commandLine",
                                                                      "Set basename for output files. File names will be composed from basename, number of frame and format\n"
                                                                      "If option is not set basename will be based on input file name."),
                                          "basename",
                                          QString());

        CommandLineOption frameOption("frames",
                                       QCoreApplication::translate("commandLine",
                                                                   "Frame(s) to export. Count is starting from 1.\n"
                                                                   "Use '-' to specify inclusive ranges. For example \"1-10\" means export frames from 1 to 10.\n"
                                                                   "Use ',' to separate ranges or individual values. For example \"1, 5-10, 15\"\n"
                                                                   "Special value 'last' means the last frame number of sprite.\n"
                                                                   "Special value 'all' means the export of all frames. It can't be used in ranges.\n"
                                                                   "By default 'all' is used."),
                                       "numbers",
                                       "all");
        parser.addOption(formatOption);
        parser.addOption(directoryOption);
        parser.addOption(baseNameOption);
        parser.addOption(frameOption);

        if (!parser.parse(subprogramArgs))
        {
            errstream << parser.errorString() << endl;
            return EXIT_FAILURE;
        }
        QByteArray format = parser.value(formatOption).toLocal8Bit();
        QString directoryName = parser.value(directoryOption);
        QString baseName = parser.value(baseNameOption);
        QString frames = parser.value(frameOption);

        QStringList positionalArgs = parser.positionalArguments();
        if (positionalArgs.size() != 1)
        {
            errstream << QCoreApplication::translate("commandLine", "Exactly one input file expected") << endl;
            return EXIT_FAILURE;
        }

        QString inputFileName = positionalArgs.first();

        Sprite sprite = SpriteLoader::load(inputFileName);
        if (!sprite.isValid())
        {
            errstream << sprite.errorString() << endl;
            return EXIT_FAILURE;
        }

        QDir dir;
        if (directoryName.isEmpty())
            dir = QDir::current();
        else
            dir = directoryName;

        if (baseName.isEmpty())
            baseName = QFileInfo(inputFileName).baseName();

        QString error;
        QVector<int> indices = parseFrameRange(frames, sprite.frameCount(), 1, &error);

        if (!error.isEmpty())
        {
            errstream << error << endl;
            return EXIT_FAILURE;
        }

        if (!exportSprite(sprite, indices, dir.path(), baseName, defaultPattern(), numberOfDigits(sprite.frameCount()), defaultFillChar(), format, &error))
        {
            errstream << error << endl;
            return EXIT_FAILURE;
        }

        return EXIT_SUCCESS;
    }
    else if (subprogram == "export-pal")
    {
        parser.addPositionalArgument("file", QCoreApplication::translate("commandLine",
                                                                         "Sprite file"));
        CommandLineOption outputOption("output",
                                 QCoreApplication::translate("commandLine", "Set output file path. If option contains only directory the file name will be based on sprite name\n"
                                                             "If option is not set the current directory will be used"), "path", QString());
        parser.addOption(outputOption);

        if (!parser.parse(subprogramArgs))
        {
            errstream << parser.errorString() << endl;
            return EXIT_FAILURE;
        }

        QStringList positionalArgs = parser.positionalArguments();
        if (positionalArgs.size() != 1)
        {
            errstream << QCoreApplication::translate("commandLine", "Exactly one input file expected") << endl;
            return EXIT_FAILURE;
        }

        QString output = parser.value(outputOption);

        QString inputFileName = positionalArgs.first();
        QFileInfo fileInfo(inputFileName);

        QString filePath;

        if (output.isEmpty())
        {
            filePath = QDir::current().filePath(fileInfo.baseName() + ".pal");
        }
        else
        {
            QFileInfo outputInfo(output);
            if (outputInfo.isDir())
            {
                filePath = outputInfo.dir().filePath(fileInfo.baseName() + ".pal");
            }
            else
            {
                filePath = output;
            }
        }

        Sprite sprite = SpriteLoader::load(inputFileName);
        if (!sprite.isValid())
        {
            errstream << sprite.errorString() << endl;
            return EXIT_FAILURE;
        }

        QString error;
        PlainPaletteHandler handler;
        if (!exportPalette(sprite.colormap(), filePath, &handler, &error))
        {
            errstream << error << endl;
            return EXIT_FAILURE;
        }

        return EXIT_SUCCESS;
    }
    else if (subprogram == "info")
    {
        CommandLineOption frameOption("frame",
                                       QCoreApplication::translate("commandLine",
                                                                   "Show information about particular frame only. Count is starting from 1."),
                                       "number");
        parser.addOption(frameOption);
        parser.addPositionalArgument("file", QCoreApplication::translate("commandLine",
                                                                         "Sprite file"));
        if (!parser.parse(subprogramArgs))
        {
            errstream << parser.errorString() << endl;
            return EXIT_FAILURE;
        }

        QStringList positionalArgs = parser.positionalArguments();
        if (positionalArgs.size() != 1)
        {
            errstream << QCoreApplication::translate("commandLine", "Exactly one input file expected") << endl;
            return EXIT_FAILURE;
        }

        QString inputFileName = positionalArgs.first();
        Sprite sprite = SpriteLoader::load(inputFileName);
        if (!sprite.isValid())
        {
            errstream << sprite.errorString() << endl;
            return EXIT_FAILURE;
        }

        QString frame = parser.value(frameOption);
        if (!frame.isEmpty())
        {
            bool ok;
            int frameNum = frame.toInt(&ok);

            if (!ok || frameNum < 1 || frameNum > sprite.frameCount())
            {
                errstream << QCoreApplication::translate("commandLine", "Invalid frame number %1").arg(frame) << endl;
                return EXIT_FAILURE;
            }

            QStringList infoList = sprite.stringFrameInfo(frameNum-1);
            foreach (QString str, infoList) {
                outstream << str << endl;
            }
        }
        else
        {
            QStringList infoList = sprite.stringSpriteInfo();

            foreach (QString str, infoList) {
                outstream << str << endl;
            }
        }

        return EXIT_SUCCESS;
    }
    else
    {
        errstream << QCoreApplication::translate("commandLine", "Unknown subprogram %1").arg(subprogram) << endl;
        return EXIT_FAILURE;
    }

    return EXIT_FAILURE;
}


CommandLineParser::CommandLineParser(const QString &programName)
{
    if (programName.isEmpty())
        _programName = QCoreApplication::arguments().first();
    else
        _programName = programName;
}

QString CommandLineParser::helpMessage() const
{
    QString usage = QCoreApplication::translate("commandLine", "Usage: %1 [options] %2").arg(_programName, _positionalArguments.join(" "));

    int maxLen = 6;
    foreach (CommandLineOption option, _options) {
        int len = 4 + option.name().size() + (option.valueName().isEmpty() ? 0 : option.valueName().size()+2);
        maxLen = qMax(maxLen, len);
    }

    foreach (QString arg, _positionalArguments) {
        maxLen = qMax(maxLen, arg.size()+2);
    }

    QString optionsStr = QCoreApplication::translate("commandLine", "Options:");
    QStringList optionList;
    foreach (CommandLineOption option, _options) {
        QString str = "--" + option.name();
        if (!option.valueName().isEmpty())
            str += " <" + option.valueName() + ">";

        int diff = maxLen - str.size();
        str = "  " + str + QString(diff, QChar(' '));

        QStringList descriptionLines = option.description().split('\n');

        str += descriptionLines.first();
        for (int i=1; i<descriptionLines.size(); ++i)
        {
            str += "\n  " + QString(maxLen, QChar(' ')) + descriptionLines[i];
        }
        optionList << str;
    }

    QString argumentsStr = QCoreApplication::translate("commandLine", "Arguments:");
    QStringList argumentList;
    for (int i=0; i<_positionalArguments.size(); ++i)
    {
        int diff = maxLen - _positionalArguments[i].size();
        QString arg = "  " + _positionalArguments[i] + QString(diff, QChar(' ')) + _positionalDescriptions[i];
        argumentList << arg;
    }

    QStringList totalList;
    totalList << usage;
    totalList << optionsStr;
    totalList << optionList;
    totalList << argumentsStr;
    totalList << argumentList;

    return totalList.join("\n");
}

void CommandLineParser::showHelp(int exitCode)
{
    QTextStream errstream(stderr, QIODevice::WriteOnly | QIODevice::Text);
    errstream << helpMessage() << endl;
    exit(exitCode);
}

bool CommandLineParser::parse(const QStringList &args)
{
    for (int i=1; i<args.size(); ++i)
    {
        QString arg = args.at(i);

        if (arg.startsWith("--"))
        {
            if (arg.size() == 2)
            {
                i++;
                while(i < args.size())
                {
                    _parsedPositionalArguments.append(args.at(i));
                    return true;
                }
            }
            else if (arg == "--help")
            {
                showHelp();
            }

            QString optionName = arg;
            optionName.remove(0, 2);

            if (_options.contains(optionName))
            {
                CommandLineOption option = _options[optionName];
                if (_results.contains(option.name()) || _checked.contains(option.name()))
                {
                    _errorString = QCoreApplication::translate("commandLine", "Option %1 is already specified").arg(arg);
                    return false;
                }

                if (!option.valueName().isEmpty())
                {
                    if (i+1 >= args.size() || args[i+1].startsWith("--"))
                    {
                        _errorString = QCoreApplication::translate("commandLine", "Value for option %1 is not given").arg(arg);
                        return false;
                    }
                    else
                    {
                        _results.insert(option.name(), args[i+1]);
                        i++;
                    }
                }
                else
                {
                    _checked.insert(option.name());
                }
            }
            else
            {
                _errorString = QCoreApplication::translate("commandLine", "Unknown option %1").arg(arg);
                return false;
            }
        }
        else
        {
            _parsedPositionalArguments.append(args[i]);
        }
    }
    return true;
}
