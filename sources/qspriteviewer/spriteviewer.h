/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef WIDGET_H
#define WIDGET_H

#include <QByteArray>
#include <QComboBox>
#include <QDockWidget>
#include <QFile>
#include <QGroupBox>
#include <QMainWindow>
#include <QMenu>
#include <QLabel>
#include <QSpinBox>
#include <QStringList>
#include <QToolBar>

#include "sprite.h"
#include "spritelabel.h"
#include "spriteframelist.h"

class SpriteViewer : public QMainWindow
{
    Q_OBJECT

public:
    SpriteViewer(QWidget *parent = 0);
    ~SpriteViewer();

    void loadFromFile(const QString& fileName);
    bool loadFromPak(QByteArray pakData, const QString& fileName, const QString& name, const QSize& pos);
    void onLoad(const Sprite& sprite);

public slots:
    void loadSpriteDialog();

    void spriteCreatorWizard();

    void exportFrameDialog(int frame, const QString& extension);
    void exportSequenceDialog();

    void zoomIn();
    void zoomOut();
    void zoomOriginal();

    void playOnce();
    void stopPlayOnce();
    void changeFpsDialog();

    void about();

    void commandLineUsageDialog();

    void setFrameStatus(int frame);
    void setScaleStatus(int scale);
    void setFpsStatus(int fps);

    void paletteDialog();

    void pakDialog();

protected:
    void closeEvent(QCloseEvent* event);
    void wheelEvent(QWheelEvent* event);

    void prevSprite();
    void nextSprite();

private slots:
    void loadSpriteHint(int index);
    void loadRecent(QAction* action);

private:
    static const int maxScaleIndex;
    static const int maxHistorySize;
    static const int scaleStep;

    void addRecent(const QString& fileName);

    void updateRecentMenu();
    void updateSpriteInfo();

    SpriteLabel* _label;
    SpriteFrameList* _frameList;
    QDockWidget* _spriteFrameDock;

    QLabel* _statusFrame;
    QLabel* _statusScale;
    QLabel* _statusFps;
    QLabel* _spriteInfo;
    QGroupBox* _spriteInfoBox;
    QDockWidget* _spriteInfoDock;

    QToolBar* _spriteFileBar;
    QComboBox* _spritesComboBox;

    QMenu* _recent;
    QAction* _animated;
    QAction* _playOnce;
    QAction* _nextFrame;
    QAction* _prevFrame;

    QByteArray _pakData;
    QString _pakFileName;
};

#endif // WIDGET_H
