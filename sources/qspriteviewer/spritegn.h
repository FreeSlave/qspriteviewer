/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
//
// spritegn.h: header file for sprite generation program
//

// **********************************************************
// * This file must be identical in the spritegen directory *
// * and in the Quake directory, because it's used to       *
// * pass data from one to the other via .spr files.        *
// **********************************************************

#ifndef SPRITEGN_H
#define SPRITEGN_H

#define SPRITE_QUAKE_VERSION	1
#define SPRITE_HL_VERSION	2
#define SPRITE_QUAKE2_VERSION 2
#define SPRITE32_VERSION	32

// must match definition in modelgen.h
#ifndef SYNCTYPE_T
#define SYNCTYPE_T
typedef enum {ST_SYNC=0, ST_RAND } synctype_t;
#endif

typedef struct {
    int			ident;
    int			version;
    int			type;
    int			texFormat;
    float		boundingradius;
    int			width;
    int			height;
    int			numframes;
    float		beamlength;
    synctype_t	synctype;
} dsprite_t;

typedef struct {
    int			ident;
    int			version;
    int			type;
    float		boundingradius;
    int			width;
    int			height;
    int			numframes;
    float		beamlength;
    synctype_t	synctype;
} dquakesprite_t;

enum {
    SPR_VP_PARALLEL_UPRIGHT	=	0,
    SPR_FACING_UPRIGHT		=	1,
    SPR_VP_PARALLEL			=	2,
    SPR_ORIENTED			=	3,
    SPR_VP_PARALLEL_ORIENTED=	4,
    SPR_LABEL               =   5,
    SPR_LABEL_SCALE         =   6,
    SPR_OVERHEAD            =   7
};

enum {
    SPR_NORMAL				=	0,
    SPR_ADDITIVE			=	1,
    SPR_INDEXALPHA			=	2,
    SPR_ALPHTEST			=	3
};

typedef struct dsprite2frame_s
{
    int		width, height;
    int		origin_x, origin_y;		// raster coordinates inside pic
    char	name[64];				// name of pcx file
} dsprite2frame_t;

typedef struct dsprite2_s
{
    int				ident;
    int				version;
    int				numframes;
} dsprite2_t;

typedef struct {
    int			origin[2];
    int			width;
    int			height;
} dspriteframe_t;

typedef struct {
    int			numframes;
} dspritegroup_t;

typedef struct {
    float	interval;
} dspriteinterval_t;

typedef enum { SPR_SINGLE=0, SPR_GROUP } spriteframetype_t;

typedef struct {
    spriteframetype_t	type;
} dspriteframetype_t;

enum {
    IDSPRITEHEADER = (('P'<<24)+('S'<<16)+('D'<<8)+'I'),
    IDSPRITEHEADER2 = (('2'<<24)+('S'<<16)+('D'<<8)+'I')
};

void makeHostEndians(dsprite_t &header);
void makeHostEndians(dquakesprite_t& header);
void makeHostEndians(dspriteframetype_t& frameType);
void makeHostEndians(dspriteframe_t& frame);

void makeHostEndians(dsprite2_t& header);
void makeHostEndians(dsprite2frame_t& frameImage);

void makeLittleEndians(dsprite_t& header);
void makeLittleEndians(dquakesprite_t& header);
void makeLittleEndians(dspriteframetype_t& frameType);
void makeLittleEndians(dspriteframe_t& frame);

void makeLittleEndians(dsprite2_t& header);
void makeLittleEndians(dsprite2frame_t& frameImage);


#endif // SPRITEGN_H
