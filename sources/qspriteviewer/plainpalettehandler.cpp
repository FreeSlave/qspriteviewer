#include "plainpalettehandler.h"

PlainPaletteHandler::PlainPaletteHandler()
{
}

QString PlainPaletteHandler::name() const
{
    return "Palette";
}

QString PlainPaletteHandler::extension() const
{
    return "pal";
}

bool PlainPaletteHandler::canLoad(QIODevice *device) const
{
    return device && device->isOpen() && device->isReadable();
}

QVector<QRgb> PlainPaletteHandler::readFromDevice(QIODevice *device) const
{
    const int size = 768;
    QByteArray arr = device->read(size);
    if (arr.size() != size)
        return QVector<QRgb>();
    QVector<QRgb> toReturn(256);
    const uchar* data = reinterpret_cast<const uchar*>(arr.constData());

    for (int i=0; i<toReturn.size(); ++i)
        toReturn[i] = qRgb(data[i*3+0], data[i*3+1], data[i*3+2]);
    return toReturn;
}

bool PlainPaletteHandler::writeToDevice(QIODevice *device, const QVector<QRgb> &pal) const
{
    if (device && device->isOpen() && device->isWritable())
    {
        QByteArray arr(pal.size()*3, '\0');
        uchar* data = reinterpret_cast<uchar*>(arr.data());

        for (int i=0; i<pal.size(); ++i)
        {
            QRgb rgb = pal.at(i);
            data[i*3+0] = qRed(rgb);
            data[i*3+1] = qGreen(rgb);
            data[i*3+2] = qBlue(rgb);
        }

        return device->write(arr) == arr.size();
    }
    return false;
}
