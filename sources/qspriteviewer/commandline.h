/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef COMMANDLINE_H
#define COMMANDLINE_H

#include <QString>
#include <QVector>
#include <QMap>
#include <QSet>
#include <QStringList>
#include <QTextStream>

#include <cstdlib>

QString optionValue(const QString& option, const QString& defaultValue);
QString optionValue(const QStringList& args, const QString& option, const QString& defaultValue);

int commandLine(int consoleArg);
void mainHelp(QTextStream& outstream);
QString helpMessage();
void showHelp();


class CommandLineOption
{
public:
    CommandLineOption() {}
    CommandLineOption(const QString& name, const QString& description, const QString& valueName = QString(), const QString& defaultValue = QString()) :
        _name(name), _description(description), _valueName(valueName), _defaultValue(defaultValue)
    {

    }

    inline QString name() const {
        return _name;
    }
    inline QString description() const {
        return _description;
    }
    inline QString valueName() const {
        return _valueName;
    }
    inline QString defaultValue() const {
        return _defaultValue;
    }

private:
    QString _name;
    QString _description;
    QString _valueName;
    QString _defaultValue;
};

class CommandLineParser
{
public:
    CommandLineParser(const QString& programName = QString());

    inline void addPositionalArgument(const QString& name, const QString& description)
    {
        _positionalArguments.append(name);
        _positionalDescriptions.append(description);
    }
    inline void addOption(const CommandLineOption& option) {
        _options.insert(option.name(), option);
    }
    inline QStringList positionalArguments() const {
        return _parsedPositionalArguments;
    }
    inline QString errorString() const {
        return _errorString;
    }
    inline bool isSet(const CommandLineOption& option) {
        return _checked.contains(option.name());
    }
    inline QString value(const CommandLineOption& option) {
        return _results.value(option.name(), option.defaultValue());
    }

    inline void setProgramName(const QString& name) {
        _programName = name;
    }

    void showHelp(int exitCode = EXIT_SUCCESS);
    QString helpMessage() const;
    bool parse(const QStringList& args);

private:

    bool hasOption(const QString& optionName);

    QString _programName;
    QSet<QString> _checked;
    QMap<QString, QString> _results;
    QMap<QString, CommandLineOption> _options;
    QStringList _positionalArguments;
    QStringList _positionalDescriptions;
    QStringList _parsedPositionalArguments;
    QString _errorString;
};

#endif // COMMANDLINE_H
