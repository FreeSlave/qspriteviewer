/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef PALETTEWIDGET_H
#define PALETTEWIDGET_H

#include <QWidget>
#include <QVector>
#include <QColor>
#include <QPen>
#include <QSize>

class PaletteWidget : public QWidget
{
    Q_OBJECT
public:
    PaletteWidget(QWidget *parent = 0);

    PaletteWidget(const QVector<QRgb>& colorTable, QWidget *parent = 0);
    ~PaletteWidget();

    void setColorTable(const QVector<QRgb>& colorTable);

    QVector<QRgb> colorTable() const {
        return _colorTable;
    }
    int colorCount() const {
        return _colorTable.size();
    }

    void resizeColorTable(int i);

    QRgb color(int index) const {
        return _colorTable.at(index);
    }

    QSize cellSize() const;
    void setCellSize(const QSize& size);
    QSize sizeHint() const;

    QRgb currentColor() const {
        if (currentIndex() >= 0)
            return color(currentIndex());
        return 0;
    }

    int currentIndex() const {
        return _chosen;
    }
    QPen selectionPen() const {
        return _selectionPen;
    }

    void setSelectionPen(const QPen& selectionPen);

    QPen framePen() const {
        return _framePen;
    }
    void setFramePen(const QPen& framePen);
    int columnNumber() const;

public slots:
    void setColor(int index, QRgb rgb);
    void setCurrent(int index);
    void resetSelection();

signals:
    void colorChosen(int, QRgb);
protected:
    void keyPressEvent(QKeyEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void paintEvent(QPaintEvent* event);

    virtual QColor oppositeColor(const QColor& color) const;

    int indexFromPos(const QPoint& pos) const;
    QPoint posFromIndex(int index) const;

    static int integerSqrt(int i);

    int cellsX() const;
    int cellsY() const;
    int cellsWidth() const;
    int cellsHeight() const;

private:
    void construct();

    int _chosen;
    QSize _cellSize;
    QPen _selectionPen;
    QPen _framePen;
    QVector<QRgb> _colorTable;
};

#endif // PALETTEWIDGET_H
