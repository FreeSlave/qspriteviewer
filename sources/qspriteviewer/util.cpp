/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#include "util.h"

#include <QDir>
#include <QFileInfo>
#include <QSettings>
#include <QtGlobal>


QString addExtension(QString fileName, QString extension)
{
    if (!fileName.isEmpty())
    {
        if (QFileInfo(fileName).suffix().isEmpty())
        {
            if (fileName[fileName.size()-1] != '.')
                fileName += '.';
            fileName += extension;
        }
    }
    return fileName;
}

int numberOfDigits(int n)
{
    QString str = QString::number(n);
    int number = str.size();
    if (str.at(0) == 1 && str.size() > 1)
    {
        for (int i=1; i<str.size(); ++i)
        {
            if (str.at(i) != '0')
                return number;
        }
        return number - 1;
    }
    return number;
}
