/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef SPRITEHANDLER_H
#define SPRITEHANDLER_H

#include <QColor>
#include <QIcon>
#include <QIODevice>
#include <QPixmap>
#include <QString>
#include <QStringList>
#include <QVariant>

#include "sprite.h"

class SpriteHandler
{
public:
    virtual QString formatName() const = 0;
    virtual QString extension() const = 0;
    virtual QString shortIdentifier() const = 0;
    virtual bool isPaletteRequired() const = 0;
    virtual QVector<QRgb> defaultPalette() const = 0;
    virtual bool canLoad(QIODevice* device) const = 0;
    virtual Sprite load(QIODevice* device, const QString& fileName = QString()) const = 0;
    virtual QString write(const Sprite& sprite, QIODevice* device, const QString& fileName = QString()) const = 0;
    virtual QStringList stringSpriteInfo(const Sprite& sprite) const = 0;
    virtual QStringList stringFrameInfo(const SpriteFrame& frame) const = 0;
    virtual QVariant generateFrameInfo(const QImage& image) const = 0;
    virtual QVariant generateSpriteInfo(const QVector<SpriteFrame>& frames) const = 0;
    virtual QIcon icon() const = 0;
    virtual int requiredPaletteSize() const = 0;
    virtual QImage convertImage(const QImage& image, const QVector<QRgb>& palette) const = 0;
    virtual QPixmap makeTransparent(const QImage& image, const QColor& background, const QVariant& spriteInfo) const = 0;
};

#endif // SPRITEHANDLER_H
