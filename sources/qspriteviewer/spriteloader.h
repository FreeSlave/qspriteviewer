/*
QSpriteViewer
Copyright (C) 2014 Chistokhodov Roman (aka FreeSlave)
Source code repository: https://bitbucket.org/FreeSlave/qspriteviewer

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
*/

#ifndef SPRITELOADER_H
#define SPRITELOADER_H

#include "spritehandler.h"
#include <QList>

class SpriteLoader
{
private:
    static QList<const SpriteHandler*> _handlers;
public:
    static void addHandler(const SpriteHandler* handler);
    static Sprite load(QIODevice* device, const QString& fileName = QString());
    static Sprite load(const QString& fileName);
    static QList<const SpriteHandler*> handlers();
    static const SpriteHandler* handlerByName(const QString &name);
    static const SpriteHandler* handlerByShortIdentifier(const QString& id);
};

#endif // SPRITELOADER_H
