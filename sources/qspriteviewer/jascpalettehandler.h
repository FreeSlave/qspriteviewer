#ifndef JASCPALETTEHANDLER_H
#define JASCPALETTEHANDLER_H

#include "palettehandler.h"

class JASCPaletteHandler : public PaletteHandler
{
public:
    JASCPaletteHandler();

    QString name() const;
    QString extension() const;
    bool canLoad(QIODevice* device) const;
    QVector<QRgb> readFromDevice(QIODevice* device) const;
    bool writeToDevice(QIODevice* device, const QVector<QRgb>& pal) const;
};

#endif // JASCPALETTEHANDLER_H
