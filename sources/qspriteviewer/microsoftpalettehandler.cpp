#include "microsoftpalettehandler.h"

#include <QDataStream>
#include <cstring>

namespace
{
const char* const RIFF = "RIFF";
const char* const PAL = "PAL ";
}

MicrosoftPaletteHandler::MicrosoftPaletteHandler()
{
}

QString MicrosoftPaletteHandler::name() const
{
    return "Microsoft palette";
}

QString MicrosoftPaletteHandler::extension() const
{
    return "pal";
}

bool MicrosoftPaletteHandler::canLoad(QIODevice *device) const
{
    if (device && device->isOpen() && device->isReadable())
    {
        QDataStream stream(device);
        stream.setByteOrder(QDataStream::LittleEndian);

        Header header;
        int n = stream.readRawData(reinterpret_cast<char*>(&header), sizeof(header));
        if (n == sizeof(header) && strncmp(header.riff, RIFF, 4) == 0 && strncmp(header.type, PAL, 4) == 0)
            return true;
    }
    return false;
}

QVector<QRgb> MicrosoftPaletteHandler::readFromDevice(QIODevice *device) const
{
    QDataStream stream(device);
    stream.setByteOrder(QDataStream::LittleEndian);

    Header header;

    int n = stream.readRawData(reinterpret_cast<char*>(&header), sizeof(header));
    if (n != sizeof(header))
        return QVector<QRgb>();

    if (header.entries <= 0)
        header.entries = 256;

    QVector<QRgb> toReturn(header.entries);

    for (int i=0; i<header.entries; ++i)
    {
        quint8 red, green, blue, flags;
        stream >> red >> green >> blue >> flags;

        if (stream.status() != QDataStream::Ok)
            return QVector<QRgb>();

        toReturn[i] = qRgb(red, green, blue);
    }

    return toReturn;
}

bool MicrosoftPaletteHandler::writeToDevice(QIODevice *device, const QVector<QRgb> &pal) const
{
    if (device && device->isOpen() && device->isWritable())
    {
        QDataStream stream(device);
        stream.setByteOrder(QDataStream::LittleEndian);

        int bytesSize = pal.size() * 4 + sizeof(Header);

        Header header;
        strncpy(header.riff, RIFF, 4);
        header.dataSize = bytesSize - 8;
        strncpy(header.type, PAL, 4);
        strncpy(header.data, "data", 4);
        header.chunkSize = bytesSize - 20;
        header.version = 0x300;
        header.entries = pal.size();

        int n = stream.writeRawData(reinterpret_cast<const char*>(&header), sizeof(header));
        if (n != sizeof(header))
            return false;

        for (int i=0; i<pal.size(); ++i)
        {
            QRgb rgb = pal.at(i);
            quint8 red = static_cast<quint8>(qRed(rgb));
            quint8 green = static_cast<quint8>(qGreen(rgb));
            quint8 blue = static_cast<quint8>(qBlue(rgb));

            stream << red << green << blue << static_cast<quint8>(0);

            if (stream.status() != QDataStream::Ok)
                return false;
        }
        return true;
    }
    return false;
}
