
TARGET = pcx
TEMPLATE = lib
CONFIG += plugin static

SOURCES += pcx.cpp

HEADERS += pcx.h

greaterThan(QT_MAJOR_VERSION, 4) {
    CONFIG(debug, debug|release) {
        QTDIRNAME = qt5_debug
    } else {
        QTDIRNAME = qt5
        DEFINES += QT_NO_DEBUG_OUTPUT
    }
} else {
    CONFIG(debug, debug|release) {
        QTDIRNAME = qt4_debug
    } else {
        QTDIRNAME = qt4
        DEFINES += QT_NO_DEBUG_OUTPUT
    }
}

REPODIR = $$PWD/../../..

OBJECTS_DIR = $$REPODIR/intermediate/$$QTDIRNAME/$$TARGET
MOC_DIR = $$OBJECTS_DIR
RCC_DIR = $$OBJECTS_DIR
DESTDIR = $$REPODIR/build/$$QTDIRNAME/plugins
