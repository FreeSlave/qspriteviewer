/* This file is part of the KDE project
   Copyright (C) 2002-2003 Nadeem Hasan <nhasan@kde.org>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the Lesser GNU General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.
*/

#ifndef PCX_H
#define PCX_H

#include <QImageIOPlugin>

class PCXPlugin : public QImageIOPlugin
{
#if QT_VERSION >= 0x050000
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QImageIOHandlerFactoryInterface" FILE "pcx.json")
#endif
public:
    PCXPlugin(QObject* parent = 0);
#if QT_VERSION < 0x050000
    QStringList keys() const;
#endif
    Capabilities capabilities(QIODevice *device, const QByteArray &format) const;
    QImageIOHandler *create(QIODevice *device, const QByteArray &format = QByteArray()) const;
};

class PCXHandler : public QImageIOHandler
{
public:
    PCXHandler();

    bool canRead() const;
    bool read(QImage *image);
    bool write(const QImage &image);

    QByteArray name() const;

    static bool canRead(QIODevice *device);
};

#endif // PCX_H

/* vim: et sw=2 ts=2
*/
